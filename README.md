# README #

This repository presents the work in progress for the ARRANGE.

### How do I get set up? ###

In order to compile the Code, a Linux system (tested on Ubuntu 18.04) is needed.
The installation is started by running the initialize_workspace.sh script. Please create a folder named **workspace** in your home folder, copy the script into this folder, make it executable, and 
replace **HOME_FOLDER_NAME** in line 40 with you current home folder name. Afterwards, run the script in the **workspace** folder.
It will install [ROS](https://www.ros.org/) and neccessary dependencies. After the successfull exectuion of the script, navigate to the folder **src/arrange**. The repository can be build by running **catkin build --this**.
To enable ROS to find the package and the executable, run **source ~/.bashrc**.

To run the test scenario execute **rosrun arrange arrange**. 

