#include "view/lists/EdgesList.h"

#include <algorithm>

EdgesList::EdgesList(wxWindow* parent, wxWindowID id)
        : wxListCtrl(parent, id, wxDefaultPosition, wxDefaultSize,
                  wxLC_REPORT | wxLC_VIRTUAL | wxLB_SINGLE | wxLC_HRULES | wxLC_VRULES | wxLC_NO_SORT_HEADER | wxLC_SORT_DESCENDING)
{
    this->SetItemCount(0);
    this->EnableAlternateRowColours(true);

    wxListItem startConceptColumn;
    startConceptColumn.SetId(0);
    startConceptColumn.SetText(_("Start Concept"));
    startConceptColumn.SetWidth(300);
    this->InsertColumn(0, startConceptColumn);

    wxListItem relationColumn;
    relationColumn.SetId(1);
    relationColumn.SetText(_("Relation"));
    relationColumn.SetWidth(300);
    this->InsertColumn(1, relationColumn);

    wxListItem endConceptColumn;
    endConceptColumn.SetId(2);
    endConceptColumn.SetText(_("End Concept"));
    endConceptColumn.SetWidth(300);
    this->InsertColumn(2, endConceptColumn);

    wxListItem weightColumn;
    weightColumn.SetId(3);
    weightColumn.SetText(_("Weight"));
    weightColumn.SetWidth(300);
    this->InsertColumn(3, weightColumn);

    wxListItem idColumn;
    idColumn.SetId(4);
    idColumn.SetText(_("ID"));
    idColumn.SetWidth(300);
    this->InsertColumn(4, idColumn);
}

wxString EdgesList::OnGetItemText(long item, long column) const
{
    switch (column) {
    case 0:
        return std::get<0>(this->edges.at(item)); // no break needed
    case 1:
        return std::get<1>(this->edges.at(item)); // no break needed
    case 2:
        return std::get<2>(this->edges.at(item)); // no break needed
    case 3:
        return std::to_string(std::get<3>(this->edges.at(item))); // no break needed
    case 4:
        return std::to_string(std::get<4>(this->edges.at(item))); // no break needed
    default:
        wxFAIL_MSG("Invalid column index in EdgesList::OnGetItemText");
    }
    return "";
}

void EdgesList::addEdge(const std::string& startConcept, const std::string& relation, const std::string& endConcept, double weight, long id)
{
    // find the correct sorted insert position
    auto toInsert = std::tuple<std::string, std::string, std::string, double, long>(startConcept, relation, endConcept, weight, id);
    auto insert_itr = std::lower_bound(std::begin(this->edges), std::end(this->edges), toInsert);
    // only insert if not a duplicate
    // test for end() first to make duplicate test safe
    if (insert_itr == std::end(this->edges) || *insert_itr != toInsert) {
        this->edges.insert(insert_itr, toInsert);
    }
    this->SetItemCount(this->edges.size());
}

void EdgesList::deleteEdge(const std::string& startConcept, const std::string& relation, const std::string& endConcept, long id)
{
    for (int i = 0; i < this->edges.size(); i++) {
        if (std::get<4>(this->edges.at(i)) == id) {
            this->edges.erase(this->edges.begin() + i);
            break;
        }
    }
    this->SetItemCount(this->edges.size());
}

void EdgesList::editEdge(const std::string& oldStartConcept, const std::string& newStartConcept, const std::string& oldRelation,
        const std::string& newRelation, const std::string& oldEndConcept, const std::string& newEndConcept, double weight, long id)
{
    this->deleteEdge(oldStartConcept, oldRelation, oldEndConcept, id);
    this->addEdge(newStartConcept, newRelation, newEndConcept, weight, id);
    this->SetItemCount(this->edges.size());
}

void EdgesList::addEdge(const std::tuple<std::string, std::string, std::string, double, long>& edge)
{
    auto insert_itr = std::lower_bound(std::begin(this->edges), std::end(this->edges), edge);
    // only insert if not a duplicate
    // test for end() first to make duplicate test safe
    if (insert_itr == std::end(this->edges) || *insert_itr != edge) {
        this->edges.insert(insert_itr, edge);
    }
    this->SetItemCount(this->edges.size());
}

void EdgesList::clearEdges()
{
    this->edges.clear();
    this->SetItemCount(0);
}

const std::string& EdgesList::getSelectedConcept() const
{
    return selectedConcept;
}

void EdgesList::setSelectedConcept(const std::string& sc)
{
    this->selectedConcept = sc;
}
