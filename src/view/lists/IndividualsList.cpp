#include "view/lists/IndividualsList.h"

#include <algorithm>

IndividualsList::IndividualsList(wxWindow* parent, wxWindowID id)
        : wxListCtrl(parent, id, wxDefaultPosition, wxDefaultSize,
                  wxLC_REPORT | wxLC_VIRTUAL | wxLB_SINGLE | wxLC_HRULES | wxLC_VRULES | wxLC_NO_SORT_HEADER | wxLC_SORT_DESCENDING)
{
    this->SetItemCount(0);
    this->EnableAlternateRowColours(true);

    wxListItem individualNameColumn;
    individualNameColumn.SetId(0);
    individualNameColumn.SetText(_("Individual Name"));
    individualNameColumn.SetWidth(300);
    this->InsertColumn(0, individualNameColumn);

    wxListItem individualBaseConceptColumn;
    individualBaseConceptColumn.SetId(1);
    individualBaseConceptColumn.SetText(_("Base Concept"));
    individualBaseConceptColumn.SetWidth(300);
    this->InsertColumn(1, individualBaseConceptColumn);
}

wxString IndividualsList::OnGetItemText(long item, long column) const
{
    switch (column) {
    case 0:
        return this->individuals.at(item).first; // no break needed
    case 1:
        return this->individuals.at(item).second; // no break needed
    default:
        wxFAIL_MSG("Invalid column index in IndividualsList::OnGetItemText");
    }
    return "";
}

void IndividualsList::addIndividual(const std::string& individualName, const std::string& baseConcept)
{
    // find the correct sorted insert position
    auto toInsert = std::pair<std::string, std::string>(individualName, baseConcept);
    auto insert_itr = std::lower_bound(std::begin(this->individuals), std::end(this->individuals), toInsert);
    // only insert if not a duplicate
    // test for end() first to make duplicate test safe
    if (insert_itr == std::end(this->individuals) || *insert_itr != toInsert) {
        this->individuals.insert(insert_itr, toInsert);
    }
    this->SetItemCount(this->individuals.size());
}

void IndividualsList::deleteIndividual(const std::string& individual)
{
    for (int i = 0; i < this->individuals.size(); i++) {
        if (this->individuals.at(i).first == individual) {
            this->individuals.erase(this->individuals.begin() + i);
            break;
        }
    }
    this->SetItemCount(this->individuals.size());
}

void IndividualsList::editIndividual(
        const std::string& oldIndividualName, const std::string& newIndividualName, const std::string& baseConcept)
{
    if (oldIndividualName == newIndividualName) {
        for (auto& individual : this->individuals) {
            if (individual.first == oldIndividualName) {
                individual.second = baseConcept;
                this->SetItemCount(this->individuals.size());
                break;
            }
        }
    } else {
        this->deleteIndividual(oldIndividualName);
        this->addIndividual(newIndividualName, baseConcept);
    }
}

void IndividualsList::clearIndividuals() {
    this->individuals.clear();
    this->SetItemCount(0);
}
