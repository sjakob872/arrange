#include "view/lists/FacetsList.h"

FacetsList::FacetsList(wxWindow* parent, wxWindowID id)
        : wxListCtrl(parent, id, wxDefaultPosition, wxDefaultSize,
                  wxLC_REPORT | wxLC_VIRTUAL | wxLB_SINGLE | wxLC_HRULES | wxLC_VRULES | wxLC_NO_SORT_HEADER | wxLC_SORT_DESCENDING | wxVSCROLL | wxHSCROLL)
{
    this->SetItemCount(0);
    this->EnableAlternateRowColours(true);

    wxListItem facetColumn;
    facetColumn.SetId(0);
    facetColumn.SetText(_("Facet"));
    facetColumn.SetWidth(1000);
    this->InsertColumn(0, facetColumn);
    this->SetScrollRate(10,10);
    Bind(wxEVT_SIZE, &FacetsList::OnSize, this);
}

wxString FacetsList::OnGetItemText(long item, long column) const
{
    if (column == 0) {
        return this->facets.at(item);
    } else {
        wxFAIL_MSG("Invalid column index in FacetsList::OnGetItemText");
    }
    return "";
}

void FacetsList::addFacet(const std::string& facet)
{
    // find the correct sorted insert position
    auto insert_itr = std::lower_bound(std::begin(this->facets), std::end(this->facets), facet);
    // only insert if not a duplicate
    // test for end() first to make duplicate test safe
    if (insert_itr == std::end(this->facets) || *insert_itr != facet) {
        this->facets.insert(insert_itr, facet);
    }
    this->SetItemCount(this->facets.size());
    this->AdjustScrollbars();
}

void FacetsList::addFacets(const std::vector<std::string>& facetStrings)
{
    for (const auto& facet : facetStrings) {
        addFacet(facet);
    }
}

void FacetsList::deleteFacet(const std::string& facet) {
    for (int i = 0; i < this->facets.size(); i++) {
        if (this->facets.at(i) == facet) {
            this->facets.erase(this->facets.begin() + i);
            break;
        }
    }
    this->SetItemCount(this->facets.size());
}

void FacetsList::OnSize(wxSizeEvent& event)
{
    this->SetColumnWidth(0, event.GetSize().GetWidth());
    this->AdjustScrollbars();
    event.Skip();
}

void FacetsList::clearFacets() {
    this->facets.clear();
    this->SetItemCount(0);
}
