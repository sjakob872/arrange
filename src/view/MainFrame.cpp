#include "view/MainFrame.h"

#include "view/GUIIdentifiers.h"
#include "view/dialogs/AddIndividualDialog.h"
#include "view/dialogs/SettingsDialog.h"
#include "view/interfaces/IArrangeController.h"
#include "view/interfaces/ISolverController.h"
#include "view/lists/EdgesList.h"
#include "view/lists/FacetsList.h"
#include "view/lists/IndividualsList.h"
#include "view/tabs/FacetsTab.h"
#include "view/tabs/IndividualsTab.h"
#include "view/tabs/OntologyProgramTab.h"
#include "view/tabs/OntologyRulesTab.h"
#include "view/tabs/OntologyTab.h"
#include "view/tabs/SolutionTab.h"

#include <wx/display.h>
#include <wx/gdicmn.h>
#include <wx/txtstrm.h>
#include <wx/wfstream.h>

#include <experimental/filesystem>
#include <iomanip>

MainFrame::MainFrame(const wxString& title)
        : wxFrame(nullptr, wxID_ANY, title)
{
    wxIcon icon;
    this->resourcesPath = std::experimental::filesystem::current_path().string() + "/resources/etc/";
    icon.LoadFile(this->resourcesPath + "icon.png", wxBITMAP_TYPE_PNG);
    SetIcon(icon);
    CentreOnParent();
    this->arrangeController = nullptr;
    this->solverController = nullptr;
}

void MainFrame::initialiseStatusBar()
{
    this->statusBar = new wxStatusBar(this, wxID_ANY, wxST_SIZEGRIP | wxNO_BORDER);
    this->statusBar->SetFieldsCount(1);
    SetStatusBar(this->statusBar);
    Bind(wxEVT_SIZE, &MainFrame::OnSizeEvent, this);
    wxRect rect;
    this->statusBar->GetFieldRect(0, rect);
    this->progressGauge = new wxGauge(statusBar, wxID_ANY, 100, rect.GetPosition(), rect.GetSize(), wxGA_HORIZONTAL);
    this->progressGauge->Hide();
    this->progressTimer = new wxTimer(this, GUIIdentifiers::PROGRESS_BAR_TIMER);
}

void MainFrame::initialiseMenuBar()
{
    this->fileMenu = new wxMenu();
    this->fileMenu->Append(GUIIdentifiers::CREATE_ONTOLOGY, _T("&Create Ontology\tAlt-C"), _T("Create an Ontology from CN5"));
    this->fileMenu->Append(GUIIdentifiers::NEW_ONTOLOGY, _T("&New Ontology\tAlt-N"), _T("Create an Empty Ontology"));
    this->fileMenu->Append(GUIIdentifiers::LOAD_ONTOLOGY_FILE, _T("&Load Ontology\tAlt-L"), _T("Load an Ontology"));
    this->fileMenu->Append(GUIIdentifiers::LOAD_FACET_FILE, _T("Load &Facets\tAlt-F"), _T("Load Facets"));
    this->fileMenu->FindChildItem(GUIIdentifiers::LOAD_FACET_FILE)->Enable(false);
    this->fileMenu->Append(GUIIdentifiers::LOAD_INDIVIDUALS_FILE, _T("Load &Individuals\tAlt-I"), _T("Load Individuals"));
    this->fileMenu->FindChildItem(GUIIdentifiers::LOAD_INDIVIDUALS_FILE)->Enable(false);
    this->fileMenu->Append(GUIIdentifiers::FILE_QUIT, _T("E&xit\tAlt-X"), _T("Quit this program"));

    this->solveMenu = new wxMenu();
    this->solveMenu->Append(GUIIdentifiers::SOLVE, _T("Solve &Ontology\tAlt-O"), _T("Show about dialog"));
    this->solveMenu->FindChildItem(GUIIdentifiers::SOLVE)->Enable(false);

    this->settingsMenu = new wxMenu();
    this->settingsMenu->Append(GUIIdentifiers::SETTINGS, _T("&Settings...\tAlt-S"), _T("Show Settings dialog"));
    this->settingsMenu->FindChildItem(GUIIdentifiers::SETTINGS)->Enable(false);

    this->helpMenu = new wxMenu();
    this->helpMenu->Append(GUIIdentifiers::HELP_ABOUT, _T("&About...\tF1"), _T("Show about dialog"));

    this->menuBar = new wxMenuBar();
    this->menuBar->Append(this->fileMenu, _T("File"));
    this->menuBar->Append(this->solveMenu, _T("Solver"));
    this->menuBar->Append(this->settingsMenu, _T("Extraction"));
    this->menuBar->Append(this->helpMenu, _T("Help"));
    SetMenuBar(this->menuBar);
}

void MainFrame::initialiseBook()
{
    /**
     * Create Ontology Tab
     */
    this->book = new wxBookCtrl(this, GUIIdentifiers::BOOK_CTRL);
    this->ontologyTab = new OntologyTab(this->book);
    this->ontologyTab->init(this->arrangeController);
    this->book->AddPage(this->ontologyTab, _T("Ontology Editor"), true);

    /**
     * Create Facets Tab
     */
    this->facetsTab = new FacetsTab(this->book);
    this->facetsTab->init(this->arrangeController);
    this->book->AddPage(this->facetsTab, _T("Facets"), false);

    /**
     * Create Individuals Tab
     */
    this->individualsTab = new IndividualsTab(this->book);
    this->individualsTab->init(this->arrangeController);
    this->book->AddPage(this->individualsTab, _T("Individuals"), false);

    /**
     * Create Ontology ASP Program Tab
     */
    this->ontologyProgramTab = new OntologyProgramTab(this->book);
    this->ontologyProgramTab->init();
    this->book->AddPage(this->ontologyProgramTab, _T("ASP Ontology"), false);

    /**
     * Create Ontology Rules Tab
     */
    this->ontologyRulesTab = new OntologyRulesTab(this->book);
    this->ontologyRulesTab->init();
    this->book->AddPage(this->ontologyRulesTab, _T("Ontology Rules"), false);

    /**
     * Solve Tab
     */
    this->solutionTab = new SolutionTab(this->book);
    this->solutionTab->init();
    this->book->AddPage(this->solutionTab, _T("Solution"), false);
}

void MainFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
    Close(true);
}

void MainFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{
    wxString msg;
    msg.Printf(_T("About.\n")
               _T("Welcome to Arrange, Arrange - Answer pRogRAmming oNtoloGy crEator."));

    wxMessageBox(msg, _T("About Arrange"), wxOK | wxICON_INFORMATION, this);
}

void MainFrame::OnCreateOntology(wxCommandEvent& WXUNUSED(event))
{
    clearLists();
    wxTextEntryDialog createDialog(this, _("Root Concept:"));

    createDialog.SetTextValidator(wxFILTER_ALPHA);
    createDialog.SetValue(wxString("thing"));
    if (createDialog.ShowModal() == wxID_CANCEL) {
        return;
    }
    std::string rootConcept;
    if (createDialog.GetValue().IsEmpty()) {
        rootConcept = "thing";
    } else {
        rootConcept = createDialog.GetValue().ToStdString();
    }
    this->arrangeController->createCN5Ontology(rootConcept);
    startProgressGauge();
}

void MainFrame::clearLists() const
{
    this->ontologyTab->getConceptListbox()->Clear();
    this->ontologyTab->getEdgesListControl()->clearEdges();
    this->facetsTab->getFacetsListControl()->clearFacets();
    this->individualsTab->getIndividualsListControl()->clearIndividuals();
}

void MainFrame::OnLoadOntology(wxCommandEvent& WXUNUSED(event))
{
    wxFileDialog openFileDialog(this, _("Open ASP Ontology"), "~", "", "Lp files (*.lp)|*.lp", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
    if (openFileDialog.ShowModal() == wxID_CANCEL) {
        return;
    }
    openFileDialog.Center(wxBOTH);
    wxFileInputStream inputStream(openFileDialog.GetPath());
    if (!inputStream.IsOk()) {
        wxLogError("Cannot open file '%s'.", openFileDialog.GetPath());
        return;
    }
    wxTextInputStream textStream(inputStream, wxT("\x09"), wxConvUTF8);
    std::string ontology = "";
    while (inputStream.IsOk() && !inputStream.Eof()) {
        wxString line = textStream.ReadLine();
        line.append("\n");
        ontology.append(line.ToStdString());
    }
    startProgressGauge();
    this->arrangeController->loadOntology(ontology);
    openFileDialog.Close();
    clearLists();
}

void MainFrame::OnLoadIndividuals(wxCommandEvent& WXUNUSED(event))
{
    wxFileDialog openFileDialog(this, _("Load ASP Individuals"), "~", "", "Lp files (*.lp)|*.lp", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
    if (openFileDialog.ShowModal() == wxID_CANCEL) {
        return;
    }
    openFileDialog.Center(wxBOTH);
    wxFileInputStream inputStream(openFileDialog.GetPath());
    if (!inputStream.IsOk()) {
        wxLogError("Cannot open file '%s'.", openFileDialog.GetPath());
        return;
    }
    wxTextInputStream textStream(inputStream, wxT("\x09"), wxConvUTF8);
    while (inputStream.IsOk() && !inputStream.Eof()) {
        wxString line = textStream.ReadLine();
        if (line.empty()) {
            continue;
        }
        this->arrangeController->createIndividual(line.ToStdString(), true);
    }
    openFileDialog.Close();
}

void MainFrame::OnLoadFacets(wxCommandEvent& event)
{
    wxFileDialog openFileDialog(this, _("Load ASP Facets"), "~", "", "Lp files (*.lp)|*.lp", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
    if (openFileDialog.ShowModal() == wxID_CANCEL) {
        return;
    }
    openFileDialog.Center(wxBOTH);
    wxFileInputStream inputStream(openFileDialog.GetPath());
    if (!inputStream.IsOk()) {
        wxLogError("Cannot open file '%s'.", openFileDialog.GetPath());
        return;
    }
    wxTextInputStream textStream(inputStream, wxT("\x09"), wxConvUTF8);
    std::vector<std::string> facets;
    while (inputStream.IsOk() && !inputStream.Eof()) {
        wxString line = textStream.ReadLine();
        if (line.empty()) {
            continue;
        }
        facets.push_back(line.ToStdString());
    }
    this->arrangeController->createFacets(facets, false);
    openFileDialog.Close();
}

void MainFrame::OnSolve(wxCommandEvent& WXUNUSED(event))
{
    startProgressGauge();
    this->solverController->solve();
    stopProgressGauge();
    std::vector<int> stats = this->solverController->getStatistics();
    std::stringstream stream;
    stream << std::fixed << std::setprecision(2) << stats[0] / 1000.0 << " ms";
    this->getSolutionTab()->getAddingLabel()->SetLabel(stream.str());
    stream.str("");
    stream << std::fixed << std::setprecision(2) << stats[1] / 1000.0 << " ms";
    this->getSolutionTab()->getGroundingLabel()->SetLabel(stream.str());
    stream.str("");
    stream << std::fixed << std::setprecision(2) << stats[2] / 1000.0 << " ms";
    this->getSolutionTab()->getExternalLabel()->SetLabel(stream.str());
    stream.str("");
    stream << std::fixed << std::setprecision(2) << stats[3] / 1000.0 << " ms";
    this->getSolutionTab()->getSolvingLabel()->SetLabel(stream.str());
    double sum = stats[0] + stats[1] + stats[2] + stats[3];
    stream.str("");
    stream << std::fixed << std::setprecision(2) << sum / 1000.0 << " ms";
    this->getSolutionTab()->getSumLabel()->SetLabel(stream.str());
    stream.str("");
    stream << std::fixed << std::setprecision(2) << stats[4] / 1000.0 << " ms";
    this->getSolutionTab()->getQueryLabel()->SetLabel(stream.str());
}

void MainFrame::startProgressGauge() const
{
    progressGauge->Show();
    progressTimer->Start(50);
}

void MainFrame::init(IArrangeController* controller, ISolverController* sController)
{
    this->arrangeController = controller;
    this->solverController = sController;
    this->initialiseMenuBar();
    this->initialiseBook();
    this->initialiseStatusBar();
}

void MainFrame::OnTimerEvent(wxTimerEvent& WXUNUSED(event))
{
    if (this->progressGauge) {
        int value = this->progressGauge->GetValue() + 1;
        value %= 100;
        this->progressGauge->SetValue(value);
    }
}

void MainFrame::OnNewOntology(wxCommandEvent& WXUNUSED(event))
{
    this->arrangeController->createNewOntology();
}

void MainFrame::OnSettings(wxCommandEvent& WXUNUSED(event))
{
    SettingsDialog* dialog = new SettingsDialog(this, wxID_ANY, "Extraction Settings");
    dialog->init(this->arrangeController);
}

void MainFrame::OnSizeEvent(wxSizeEvent& event)
{
    if (this->progressGauge) {
        this->progressGauge->SetSize(event.GetSize());
    }
    event.Skip();
}

void MainFrame::ontologyCreationFinished()
{
    stopProgressGauge();
    this->fileMenu->FindChildItem(GUIIdentifiers::LOAD_INDIVIDUALS_FILE)->Enable();
    this->fileMenu->FindChildItem(GUIIdentifiers::LOAD_FACET_FILE)->Enable();
    this->solveMenu->FindChildItem(GUIIdentifiers::SOLVE)->Enable();
    this->settingsMenu->FindChildItem(GUIIdentifiers::SETTINGS)->Enable();
    this->ontologyTab->enable();
    this->individualsTab->enable();
    this->ontologyRulesTab->enable();
    this->ontologyProgramTab->enable();
    this->facetsTab->enable();
}

void MainFrame::stopProgressGauge() const
{
    progressGauge->SetValue(0);
    progressTimer->Stop();
    progressGauge->Hide();
}

wxListBox* MainFrame::getConceptListbox() const
{
    return this->ontologyTab->getConceptListbox();
}

wxTextCtrl* MainFrame::getOntologyTextControl() const
{
    return this->ontologyProgramTab->getOntologyTextControl();
}

wxTextCtrl* MainFrame::getOntologyRulesTextControl() const
{
    return this->ontologyRulesTab->getOntologyRulesTextControl();
}

IndividualsTab* MainFrame::getIndividualsTab() const
{
    return this->individualsTab;
}

SolutionTab* MainFrame::getSolutionTab() const
{
    return this->solutionTab;
}

const std::string& MainFrame::getResourcesPath() const
{
    return this->resourcesPath;
}

OntologyTab* MainFrame::getOntologyTab() const
{
    return this->ontologyTab;
}

FacetsTab* MainFrame::getFacetsTab() const
{
    return this->facetsTab;
}
