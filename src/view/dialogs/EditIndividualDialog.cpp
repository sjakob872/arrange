#include "view/dialogs/EditIndividualDialog.h"

#include "view/interfaces/IArrangeController.h"

#include <iostream>
#include <utility>
#include <wx/event.h>
#include <wx/sizer.h>
#include <wx/stattext.h>

EditIndividualDialog::EditIndividualDialog(
        wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style)
        : wxDialog(parent, id, title, pos, size, style)
{
    this->createOntologyController = nullptr;
}

void EditIndividualDialog::init(IArrangeController* controller, std::string selectedIndividual, std::string selectedIndividualConcept)
{
    this->createOntologyController = controller;
    this->selectedIndividualOldName = std::move(selectedIndividual);
    this->selectedIndividualConcept = std::move(selectedIndividualConcept);
    this->Center(wxBOTH);

    wxStaticText* individualLabel = new wxStaticText(this, wxID_ANY, _T("Individual:"));
    this->individualName = new wxTextCtrl(this, wxID_ANY);
    wxBoxSizer* individualSizer = new wxBoxSizer(wxHORIZONTAL);
    individualSizer->Add(individualLabel, 1, wxCENTER, 10);
    individualSizer->Add(this->individualName, 3, wxCENTER, 10);
    this->individualName->SetValue(this->selectedIndividualOldName);

    wxStaticText* conceptLabel = new wxStaticText(this, wxID_ANY, _T("Base Concept:"));
    this->baseConcepts = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, {}, wxCB_SORT);
    for (const auto& conceptName : this->createOntologyController->getAvailableConcepts()) {
        this->baseConcepts->Append(conceptName);
    }
    this->baseConcepts->SetSelection(this->baseConcepts->FindString(this->selectedIndividualConcept));
    wxBoxSizer* conceptSizer = new wxBoxSizer(wxHORIZONTAL);
    conceptSizer->Add(conceptLabel, 1, wxCENTER, 10);
    conceptSizer->Add(this->baseConcepts, 3, wxCENTER, 10);

    wxStdDialogButtonSizer* buttonSizer = new wxStdDialogButtonSizer();

    this->editButton = new wxButton(this, wxID_ANY, "Edit");
    this->cancelButton = new wxButton(this, wxID_ANY, "Cancel");
    buttonSizer->SetAffirmativeButton(this->editButton);
    buttonSizer->SetCancelButton(this->cancelButton);
    buttonSizer->Realize();

    wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(individualSizer, 1, wxEXPAND, 10);
    sizer->Add(conceptSizer, 1, wxEXPAND, 10);
    sizer->Add(buttonSizer, 1, wxEXPAND, 10);
    this->SetSizer(sizer);
    this->cancelButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &EditIndividualDialog::OnCancel, this);
    this->editButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &EditIndividualDialog::OnEdit, this);

    if (this->ShowModal() == wxID_CANCEL) {
        return;
    }
}

void EditIndividualDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
    this->Close();
}

void EditIndividualDialog::OnEdit(wxCommandEvent& WXUNUSED(event))
{
    if (this->individualName->IsEmpty()) {
        wxColour colour;
        colour.Set(245, 75, 75);
        this->individualName->SetBackgroundColour(colour);
        return;
    }
    if (this->baseConcepts->GetSelection() == wxNOT_FOUND) {
        wxColour colour;
        colour.Set(245, 75, 75);
        this->baseConcepts->SetBackgroundColour(colour);
        return;
    }
    if (this->selectedIndividualOldName == this->individualName->GetValue().ToStdString() &&
            this->selectedIndividualConcept == this->baseConcepts->GetStringSelection().ToStdString()) {
        return;
    }
    this->createOntologyController->editIndividual(this->selectedIndividualOldName, this->individualName->GetValue().ToStdString(),
            this->baseConcepts->GetStringSelection().ToStdString());
    this->Close();
}
