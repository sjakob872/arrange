#include "view/dialogs/EditEdgeDialog.h"

#include "view/interfaces/IArrangeController.h"

#include <wx/button.h>
#include <wx/event.h>
#include <wx/sizer.h>
#include <wx/stattext.h>

EditEdgeDialog::EditEdgeDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style)
        : wxDialog(parent, id, title, pos, size, style)
{
    this->createOntologyController = nullptr;
}

void EditEdgeDialog::init(IArrangeController* controller, const std::string& startConcept, const std::string& relation,
        const std::string& endConcept, double weight, const std::string& id)
{
    this->SetSize(835, 150);
    this->createOntologyController = controller;
    this->Center(wxBOTH);

    this->selectedStartConcept = startConcept;
    this->selectedRelation = relation;
    this->selectedEndConcept = endConcept;
    this->selectedWeight = weight;
    this->selectedId = id;

    auto concepts = this->createOntologyController->getAvailableConcepts();

    wxStaticText* formConceptLabel = new wxStaticText(this, wxID_ANY, _T("From Concept:"));
    this->fromConceptNameCmbBox = new wxComboBox(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(200, 30));
    for (const auto& conceptName : concepts) {
        this->fromConceptNameCmbBox->Append(conceptName);
    }
    this->fromConceptNameCmbBox->SetValue(this->selectedStartConcept);
    wxBoxSizer* fromSizer = new wxBoxSizer(wxVERTICAL);
    fromSizer->Add(formConceptLabel, 0, wxEXPAND | wxRIGHT, 5);
    fromSizer->Add(this->fromConceptNameCmbBox, 0, wxEXPAND | wxRIGHT, 5);

    wxStaticText* relationLabel = new wxStaticText(this, wxID_ANY, _T("Relation:"));
    this->relationChoice = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxSize(200, 30), 0, {}, wxCB_SORT);
    for (const auto& rel : this->createOntologyController->getOntologyRelations()) {
        this->relationChoice->Append(rel);
    }
    this->relationChoice->SetSelection(this->relationChoice->FindString(this->selectedRelation));
    wxBoxSizer* relationSizer = new wxBoxSizer(wxVERTICAL);
    relationSizer->Add(relationLabel, 0, wxEXPAND | wxRIGHT, 5);
    relationSizer->Add(this->relationChoice, 0, wxEXPAND | wxRIGHT, 5);

    wxStaticText* toConceptLabel = new wxStaticText(this, wxID_ANY, _T("To Concept:"));
    this->toConceptNameCmbBox = new wxComboBox(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(200, 30));
    for (const auto& conceptName : concepts) {
        this->toConceptNameCmbBox->Append(conceptName);
    }
    this->toConceptNameCmbBox->SetValue(this->selectedEndConcept);
    wxBoxSizer* toSizer = new wxBoxSizer(wxVERTICAL);
    toSizer->Add(toConceptLabel, 0, wxEXPAND | wxRIGHT, 5);
    toSizer->Add(this->toConceptNameCmbBox, 0, wxEXPAND | wxRIGHT, 5);

    wxStaticText* weightLabel = new wxStaticText(this, wxID_ANY, _T("Weight:"));
    this->weightSpinCtrl = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(200, 30));
    this->weightSpinCtrl->SetValue(this->selectedWeight);
    this->weightSpinCtrl->SetDigits(1);
    this->weightSpinCtrl->SetRange(1.0, 100.0);
    this->weightSpinCtrl->SetIncrement(0.1);
    wxBoxSizer* weightSizer = new wxBoxSizer(wxVERTICAL);
    weightSizer->Add(weightLabel, 0, wxEXPAND | wxRIGHT, 5);
    weightSizer->Add(this->weightSpinCtrl, 0, wxEXPAND | wxRIGHT, 5);

    wxStdDialogButtonSizer* buttonSizer = new wxStdDialogButtonSizer();

    this->editButton = new wxButton(this, wxID_ANY, "Edit");
    this->cancelButton = new wxButton(this, wxID_ANY, "Cancel");
    buttonSizer->SetAffirmativeButton(this->editButton);
    buttonSizer->SetCancelButton(this->cancelButton);
    buttonSizer->Realize();

    wxBoxSizer* hSizer = new wxBoxSizer(wxHORIZONTAL);
    hSizer->Add(fromSizer, 0, wxEXPAND | wxALL, 0);
    hSizer->Add(relationSizer, 0, wxEXPAND | wxALL, 0);
    hSizer->Add(toSizer, 0, wxEXPAND | wxALL, 0);
    hSizer->Add(weightSizer, 0, wxEXPAND | wxALL, 0);

    wxBoxSizer* vSizer = new wxBoxSizer(wxVERTICAL);
    vSizer->Add(hSizer, 0, wxEXPAND | wxALL, 10);
    vSizer->Add(buttonSizer, 0, wxEXPAND | wxALL, 10);

    this->SetSizer(vSizer);
    this->cancelButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &EditEdgeDialog::OnCancel, this);
    this->editButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &EditEdgeDialog::OnEdit, this);

    if (this->ShowModal() == wxID_CANCEL) {
        return;
    }
}

void EditEdgeDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
    this->Close();
}

void EditEdgeDialog::OnEdit(wxCommandEvent& WXUNUSED(event))
{
    if (this->fromConceptNameCmbBox->IsTextEmpty()) {
        wxColour colour;
        colour.Set(245, 75, 75);
        this->fromConceptNameCmbBox->SetBackgroundColour(colour);
        return;
    }
    if (this->toConceptNameCmbBox->IsTextEmpty()) {
        wxColour colour;
        colour.Set(245, 75, 75);
        this->toConceptNameCmbBox->SetBackgroundColour(colour);
        return;
    }
    if (this->relationChoice->IsEmpty()) {
        wxColour colour;
        colour.Set(245, 75, 75);
        this->relationChoice->SetBackgroundColour(colour);
        return;
    }
    this->createOntologyController->editEdge(this->selectedStartConcept, this->fromConceptNameCmbBox->GetValue().ToStdString(),
            this->selectedRelation, this->relationChoice->GetStringSelection().ToStdString(), this->selectedEndConcept,
            this->toConceptNameCmbBox->GetValue().ToStdString(), this->weightSpinCtrl->GetValue(), std::stol(this->selectedId));

    this->Close();
}