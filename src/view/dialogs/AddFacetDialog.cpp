#include "view/dialogs/AddFacetDialog.h"

#include "model/FacetTypes.h"
#include "view/interfaces/IArrangeController.h"

#include <iostream>
#include <wx/button.h>
#include <wx/event.h>
#include <wx/sizer.h>
#include <wx/stattext.h>

AddFacetDialog::AddFacetDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style)
        : wxDialog(parent, id, title, pos, size, style)
{
}

void AddFacetDialog::init(IArrangeController* controller)
{
    this->SetSize(600, 500);
    this->createOntologyController = controller;
    this->Center(wxBOTH);

    wxStaticText* fromConceptLabel = new wxStaticText(this, wxID_ANY, _T("From Concept:"));
    this->fromConceptNameCmbBox = new wxComboBox(this, wxID_ANY);
    wxBoxSizer* fromConceptSizer = new wxBoxSizer(wxVERTICAL);
    for (const auto& conceptName : this->createOntologyController->getAvailableConcepts()) {
        this->fromConceptNameCmbBox->Append(conceptName);
    }
    fromConceptSizer->Add(fromConceptLabel, 1, wxCENTER, 5);
    fromConceptSizer->Add(this->fromConceptNameCmbBox, 1, wxLEFT | wxEXPAND, 5);

    wxStaticText* relationLabel = new wxStaticText(this, wxID_ANY, _T("Facet Type:"));
    this->relationChoice = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, {}, wxCB_SORT);
    for (const auto& relation : this->createOntologyController->getFacetRelations(true)) {
        this->relationChoice->Append(relation);
    }
    wxBoxSizer* relationSizer = new wxBoxSizer(wxVERTICAL);
    relationSizer->Add(relationLabel, 1, wxCENTER, 5);
    relationSizer->Add(this->relationChoice, 1, wxLEFT | wxRIGHT | wxEXPAND, 5);
    this->relationChoice->Bind(wxEVT_CHOICE, &AddFacetDialog::enable, this);

    wxStaticText* toConceptLabel = new wxStaticText(this, wxID_ANY, _T("To Concept:"));
    this->toConceptNameChoice = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, {}, wxCB_SORT);
    for (const auto& conceptName : this->createOntologyController->getAvailableConcepts()) {
        this->toConceptNameChoice->Append(conceptName);
    }
    wxBoxSizer* toConceptSizer = new wxBoxSizer(wxVERTICAL);
    toConceptSizer->Add(toConceptLabel, 1, wxCENTER, 5);
    toConceptSizer->Add(this->toConceptNameChoice, 1, wxRIGHT | wxEXPAND, 5);

    wxStaticText* typeStringLabel = new wxStaticText(this, wxID_ANY, _T("Type:"));
    this->typeStringTxtCtrl = new wxTextCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize);
    this->typeStringTxtCtrl->Disable();
    wxBoxSizer* typeStringSizer = new wxBoxSizer(wxVERTICAL);
    typeStringSizer->Add(typeStringLabel, 1, wxCENTER, 5);
    typeStringSizer->Add(this->typeStringTxtCtrl, 1, wxLEFT | wxRIGHT | wxEXPAND, 5);

    wxStaticText* valueRangeLabel = new wxStaticText(this, wxID_ANY, _T("Value Range:"));
    this->valueRangeTxtCtrl = new wxTextCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize);
    this->valueRangeTxtCtrl->Disable();
    wxBoxSizer* valueRangeSizer = new wxBoxSizer(wxVERTICAL);
    valueRangeSizer->Add(valueRangeLabel, 1, wxCENTER, 5);
    valueRangeSizer->Add(this->valueRangeTxtCtrl, 1, wxLEFT | wxEXPAND, 5);

    wxStaticText* minLabel = new wxStaticText(this, wxID_ANY, _T("Min Cardinality:"));
    this->minCardSpinCtrl = new wxSpinCtrl(this, wxID_ANY, "0", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxALIGN_RIGHT, 0, 1000);
    this->minCardSpinCtrl->Disable();
    wxBoxSizer* minSizer = new wxBoxSizer(wxVERTICAL);
    minSizer->Add(minLabel, 1, wxCENTER, 5);
    minSizer->Add(this->minCardSpinCtrl, 1, wxLEFT | wxRIGHT | wxEXPAND, 5);

    wxStaticText* maxLabel = new wxStaticText(this, wxID_ANY, _T("Max Cardinality:"));
    this->maxCardSpinCtrl = new wxSpinCtrl(this, wxID_ANY, "0", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxALIGN_RIGHT, 0, 1000);
    this->maxCardSpinCtrl->Disable();
    wxBoxSizer* maxSizer = new wxBoxSizer(wxVERTICAL);
    maxSizer->Add(maxLabel, 1, wxCENTER, 5);
    maxSizer->Add(this->maxCardSpinCtrl, 1, wxRIGHT | wxEXPAND, 5);

    wxStaticText* individualLabel = new wxStaticText(this, wxID_ANY, _T("Individual:"));
    this->individualChoice = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, {}, wxCB_SORT);
    for (const auto& individualName : this->createOntologyController->getAvailableIndividuals()) {
        this->individualChoice->Append(individualName);
    }
    this->individualChoice->Disable();
    wxBoxSizer* individualSizer = new wxBoxSizer(wxVERTICAL);
    individualSizer->Add(individualLabel, 1, wxCENTER, 5);
    individualSizer->Add(this->individualChoice, 1, wxLEFT | wxEXPAND, 5);

    wxStaticText* valueLabel = new wxStaticText(this, wxID_ANY, _T("Value:"));
    this->valueTxtCtrl = new wxTextCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize);
    this->valueTxtCtrl->Disable();
    wxBoxSizer* valueSizer = new wxBoxSizer(wxVERTICAL);
    valueSizer->Add(valueLabel, 1, wxCENTER, 5);
    valueSizer->Add(this->valueTxtCtrl, 1, wxLEFT | wxRIGHT | wxEXPAND, 5);

    wxStaticText* timeStepLabel = new wxStaticText(this, wxID_ANY, _T("Time Step:"));
    this->timeStepSpinCtrl =
            new wxSpinCtrl(this, wxID_ANY, "0", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxALIGN_RIGHT, 0, 1000);
    this->timeStepSpinCtrl->Disable();
    wxBoxSizer* timeStepSizer = new wxBoxSizer(wxVERTICAL);
    timeStepSizer->Add(timeStepLabel, 1, wxCENTER, 5);
    timeStepSizer->Add(this->timeStepSpinCtrl, 1, wxRIGHT | wxEXPAND, 5);

    wxStdDialogButtonSizer* buttonSizer = new wxStdDialogButtonSizer();

    this->createButton = new wxButton(this, wxID_ANY, "Create");
    this->cancelButton = new wxButton(this, wxID_ANY, "Cancel");
    buttonSizer->SetAffirmativeButton(this->createButton);
    buttonSizer->SetCancelButton(this->cancelButton);
    buttonSizer->Realize();

    wxBoxSizer* firstSizer = new wxBoxSizer(wxHORIZONTAL);
    firstSizer->Add(fromConceptSizer, 1, wxCENTER, 5);
    firstSizer->Add(relationSizer, 1, wxCENTER, 5);
    firstSizer->Add(toConceptSizer, 1, wxCENTER, 5);

    wxBoxSizer* secondSizer = new wxBoxSizer(wxHORIZONTAL);
    secondSizer->Add(typeStringSizer, 1, wxCENTER, 5);

    wxBoxSizer* thirdSizer = new wxBoxSizer(wxHORIZONTAL);
    thirdSizer->Add(valueRangeSizer, 1, wxCENTER, 5);
    thirdSizer->Add(minSizer, 1, wxCENTER, 5);
    thirdSizer->Add(maxSizer, 1, wxCENTER, 5);

    wxBoxSizer* fourthSizer = new wxBoxSizer(wxHORIZONTAL);
    fourthSizer->Add(individualSizer, 1, wxCENTER, 5);
    fourthSizer->Add(valueSizer, 1, wxCENTER, 5);
    fourthSizer->Add(timeStepSizer, 1, wxCENTER, 5);

    wxBoxSizer* finalSizer = new wxBoxSizer(wxVERTICAL);
    finalSizer->Add(firstSizer, 1, wxEXPAND, 10);
    finalSizer->Add(secondSizer, 1, wxEXPAND, 10);
    finalSizer->Add(thirdSizer, 1, wxEXPAND, 10);
    finalSizer->Add(fourthSizer, 1, wxEXPAND, 10);
    finalSizer->Add(buttonSizer, 1, wxEXPAND, 10);
    this->SetSizer(finalSizer);
    this->cancelButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &AddFacetDialog::OnCancel, this);
    this->createButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &AddFacetDialog::OnCreate, this);

    if (this->ShowModal() == wxID_CANCEL) {
        return;
    }
}

void AddFacetDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
    this->Close();
}

void AddFacetDialog::OnCreate(wxCommandEvent& WXUNUSED(event))
{
    if (this->fromConceptNameCmbBox->IsTextEmpty()) {
        wxColour colour;
        colour.Set(245, 75, 75);
        this->fromConceptNameCmbBox->SetBackgroundColour(colour);
        return;
    }
    if (this->toConceptNameChoice->GetCount() == 0 || this->toConceptNameChoice->GetSelection() == wxNOT_FOUND) {
        wxColour colour;
        colour.Set(245, 75, 75);
        this->toConceptNameChoice->SetBackgroundColour(colour);
        return;
    }
    if (this->relationChoice->GetSelection() == wxNOT_FOUND) {
        wxColour colour;
        colour.Set(245, 75, 75);
        this->relationChoice->SetBackgroundColour(colour);
        return;
    }
    this->createOntologyController->createFacet(this->fromConceptNameCmbBox->GetValue().ToStdString(),
            this->relationChoice->GetStringSelection().ToStdString(), this->toConceptNameChoice->GetStringSelection().ToStdString(),
            this->typeStringTxtCtrl->GetValue().ToStdString(), this->valueRangeTxtCtrl->GetValue().ToStdString(),
            this->minCardSpinCtrl->GetValue(), this->maxCardSpinCtrl->GetValue(),
            this->individualChoice->GetStringSelection().ToStdString(), this->valueTxtCtrl->GetValue().ToStdString(),
            this->timeStepSpinCtrl->GetValue());
    this->Close();
}

void AddFacetDialog::enable(wxCommandEvent& event)
{
    this->typeStringTxtCtrl->Disable();
    this->valueRangeTxtCtrl->Disable();
    this->minCardSpinCtrl->Disable();
    this->maxCardSpinCtrl->Disable();
    this->individualChoice->Disable();
    this->valueTxtCtrl->Disable();
    this->timeStepSpinCtrl->Disable();
    if (event.GetString() == facetTypes[FacetType::facetOf]) {
        return;
    } else if (event.GetString() == facetTypes[FacetType::typeOf]) {
        this->typeStringTxtCtrl->Enable();
    } else if (event.GetString() == facetTypes[FacetType::valueRangeOf]) {
        this->valueRangeTxtCtrl->Enable();
        this->minCardSpinCtrl->Enable();
        this->maxCardSpinCtrl->Enable();
    } else if (event.GetString() == facetTypes[FacetType::hasValue]) {
        this->individualChoice->Enable();
        this->valueTxtCtrl->Enable();
        this->timeStepSpinCtrl->Enable();
    } else if (event.GetString() == facetTypes[FacetType::subPropertyOf]) {
        this->timeStepSpinCtrl->Enable();
    } else if (event.GetString() == facetTypes[FacetType::domainOf]) {
        this->timeStepSpinCtrl->Enable();
    }
    event.Skip();
}
