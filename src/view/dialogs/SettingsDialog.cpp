#include "view/dialogs/SettingsDialog.h"

#include "view/interfaces/IArrangeController.h"

#include <wx/button.h>
#include <wx/event.h>
#include <wx/sizer.h>
#include <wx/stattext.h>

SettingsDialog::SettingsDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style)
        : wxDialog(parent, id, title, pos, size, style)
{
    this->createOntologyController = nullptr;
}

void SettingsDialog::init(IArrangeController* controller)
{
    this->SetSize(300, 300);
    this->createOntologyController = controller;
    this->Center(wxBOTH);

    wxStaticText* isAWeightLabel = new wxStaticText(this, wxID_ANY, _T("IsA Weight:"));
    this->isAWeightSpinCtrl = new wxSpinCtrlDouble(
            this, wxID_ANY, "2.5", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxALIGN_RIGHT, 0.0, 10.0, 2.5, 0.1);
    wxBoxSizer* isASizer = new wxBoxSizer(wxHORIZONTAL);
    isASizer->Add(isAWeightLabel, 1, wxLEFT | wxCENTER, 5);
    isASizer->Add(this->isAWeightSpinCtrl, 1, wxLEFT | wxRIGHT | wxCENTER, 5);

    wxStaticText* formOfWeightLabel = new wxStaticText(this, wxID_ANY, _T("FormOf Weight:"));
    this->formOfWeightSpinCtrl = new wxSpinCtrlDouble(
            this, wxID_ANY, "2.0", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxALIGN_RIGHT, 0.0, 10.0, 2.0, 0.1);
    wxBoxSizer* formOfSizer = new wxBoxSizer(wxHORIZONTAL);
    formOfSizer->Add(formOfWeightLabel, 1, wxLEFT | wxCENTER, 5);
    formOfSizer->Add(this->formOfWeightSpinCtrl, 1, wxLEFT | wxRIGHT | wxCENTER, 5);

    wxStaticText* synonymWeightLabel = new wxStaticText(this, wxID_ANY, _T("Synonym Weight:"));
    this->synonymWeightSpinCtrl = new wxSpinCtrlDouble(
            this, wxID_ANY, "2.0", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxALIGN_RIGHT, 0.0, 10.0, 2.0, 0.1);
    wxBoxSizer* synonymSizer = new wxBoxSizer(wxHORIZONTAL);
    synonymSizer->Add(synonymWeightLabel, 1, wxLEFT | wxCENTER, 5);
    synonymSizer->Add(this->synonymWeightSpinCtrl, 1, wxLEFT | wxRIGHT | wxCENTER, 5);

    wxStaticText* hasPropertyAWeightLabel = new wxStaticText(this, wxID_ANY, _T("HasProperty Weight:"));
    this->hasPropertyWeightSpinCtrl = new wxSpinCtrlDouble(
            this, wxID_ANY, "1.5", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxALIGN_RIGHT, 0.0, 10.0, 1.5, 0.1);
    wxBoxSizer* hasPropertySizer = new wxBoxSizer(wxHORIZONTAL);
    hasPropertySizer->Add(hasPropertyAWeightLabel, 1, wxLEFT | wxCENTER, 5);
    hasPropertySizer->Add(this->hasPropertyWeightSpinCtrl, 1, wxLEFT | wxRIGHT | wxCENTER, 5);

    wxStaticText* minRelatednessLabel = new wxStaticText(this, wxID_ANY, _T("Minimal Relatedness:"));
    this->minRelatednessSpinCtrl = new wxSpinCtrlDouble(
            this, wxID_ANY, "0.7", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxALIGN_RIGHT, 0.0, 10.0, 0.7, 0.1);
    wxBoxSizer* minRelatednessSizer = new wxBoxSizer(wxHORIZONTAL);
    minRelatednessSizer->Add(minRelatednessLabel, 1, wxLEFT | wxCENTER, 5);
    minRelatednessSizer->Add(this->minRelatednessSpinCtrl, 1, wxLEFT | wxRIGHT | wxCENTER, 5);

    wxStdDialogButtonSizer* buttonSizer = new wxStdDialogButtonSizer();

    this->applyButton = new wxButton(this, wxID_ANY, "Apply");
    this->cancelButton = new wxButton(this, wxID_ANY, "Cancel");
    buttonSizer->SetAffirmativeButton(this->applyButton);
    buttonSizer->SetCancelButton(this->cancelButton);
    buttonSizer->Realize();

    wxBoxSizer* finalSizer = new wxBoxSizer(wxVERTICAL);
    finalSizer->Add(isASizer, 1, wxEXPAND, 10);
    finalSizer->Add(formOfSizer, 1, wxEXPAND, 10);
    finalSizer->Add(synonymSizer, 1, wxEXPAND, 10);
    finalSizer->Add(hasPropertySizer, 1, wxEXPAND, 10);
    finalSizer->Add(minRelatednessSizer, 1, wxEXPAND, 10);
    finalSizer->Add(buttonSizer, 1, wxEXPAND, 10);
    this->SetSizer(finalSizer);
    this->cancelButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &SettingsDialog::OnCancel, this);
    this->applyButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &SettingsDialog::OnApply, this);

    if (this->ShowModal() == wxID_CANCEL) {
        return;
    }
}

void SettingsDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
    this->Close();
}

void SettingsDialog::OnApply(wxCommandEvent& WXUNUSED(event))
{
    this->createOntologyController->applySettings(this->isAWeightSpinCtrl->GetValue(), this->formOfWeightSpinCtrl->GetValue(),
            this->synonymWeightSpinCtrl->GetValue(), this->hasPropertyWeightSpinCtrl->GetValue(), this->minRelatednessSpinCtrl->GetValue());
    this->Close();
}
