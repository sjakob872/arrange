#include "view/dialogs/AddIndividualDialog.h"
#include "view/interfaces/IArrangeController.h"

#include <iostream>
#include <wx/event.h>
#include <wx/sizer.h>
#include <wx/stattext.h>

AddIndividualDialog::AddIndividualDialog(
        wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style)
        : wxDialog(parent, id, title, pos, size, style)
{
    this->createOntologyController = nullptr;
}

void AddIndividualDialog::init(IArrangeController* controller)
{
    this->createOntologyController = controller;
    this->Center(wxBOTH);

    wxStaticText* individualLabel = new wxStaticText(this, wxID_ANY, _T("Individual:"));
    this->individualName = new wxTextCtrl(this, wxID_ANY);
    wxBoxSizer* individualSizer = new wxBoxSizer(wxHORIZONTAL);
    individualSizer->Add(individualLabel, 1, wxCENTER, 10);
    individualSizer->Add(this->individualName, 3, wxCENTER, 10);

    wxStaticText* conceptLabel = new wxStaticText(this, wxID_ANY, _T("Base Concept:"));
    this->baseConcepts = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, {}, wxCB_SORT);
    for (const auto& conceptName : this->createOntologyController->getAvailableConcepts()) {
        this->baseConcepts->Append(conceptName);
    }
    wxBoxSizer* conceptSizer = new wxBoxSizer(wxHORIZONTAL);
    conceptSizer->Add(conceptLabel, 1, wxCENTER, 10);
    conceptSizer->Add(this->baseConcepts, 3, wxCENTER, 10);

    wxStdDialogButtonSizer* buttonSizer = new wxStdDialogButtonSizer();

    this->createButton = new wxButton(this, wxID_ANY, "Create");
    this->cancelButton = new wxButton(this, wxID_ANY, "Cancel");
    buttonSizer->SetAffirmativeButton(this->createButton);
    buttonSizer->SetCancelButton(this->cancelButton);
    buttonSizer->Realize();

    wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(individualSizer, 1, wxEXPAND, 10);
    sizer->Add(conceptSizer, 1, wxEXPAND, 10);
    sizer->Add(buttonSizer, 1, wxEXPAND, 10);
    this->SetSizer(sizer);
    this->cancelButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &AddIndividualDialog::OnCancel, this);
    this->createButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &AddIndividualDialog::OnCreate, this);

    if (this->ShowModal() == wxID_CANCEL) {
        return;
    }
}

void AddIndividualDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
    this->Close();
}

void AddIndividualDialog::OnCreate(wxCommandEvent& WXUNUSED(event))
{
    if (this->individualName->IsEmpty()) {
        wxColour colour;
        colour.Set(245, 75, 75);
        this->individualName->SetBackgroundColour(colour);
        return;
    }
    if (this->baseConcepts->GetSelection() == wxNOT_FOUND) {
        wxColour colour;
        colour.Set(245, 75, 75);
        this->baseConcepts->SetBackgroundColour(colour);
        return;
    }
    this->createOntologyController->createIndividual(
            this->individualName->GetValue().ToStdString(), this->baseConcepts->GetStringSelection().ToStdString());
    this->Close();
}
