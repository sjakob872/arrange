#include "view/panels/wxImagePanel.h"
#include <wx/dcbuffer.h>
#include <wx/window.h>

BEGIN_EVENT_TABLE(wxImagePanel, wxScrolledWindow)
// some useful events
/*
 EVT_MOTION(wxImagePanel::mouseMoved)
 EVT_LEFT_DOWN(wxImagePanel::mouseDown)
 EVT_LEFT_UP(wxImagePanel::mouseReleased)
 EVT_RIGHT_DOWN(wxImagePanel::rightClick)
 EVT_LEAVE_WINDOW(wxImagePanel::mouseLeftWindow)
 EVT_KEY_DOWN(wxImagePanel::keyPressed)
 EVT_KEY_UP(wxImagePanel::keyReleased)
EVT_MOUSEWHEEL(wxImagePanel::mouseWheelMoved)
 */

// catch paint events
EVT_PAINT(wxImagePanel::paintEvent)
// Size event
EVT_SIZE(wxImagePanel::OnSize)
END_EVENT_TABLE()

// some useful events
/*
 void wxImagePanel::mouseMoved(wxMouseEvent& event) {}
 void wxImagePanel::mouseDown(wxMouseEvent& event) {}
 void wxImagePanel::mouseWheelMoved(wxMouseEvent& event) {}
 void wxImagePanel::mouseReleased(wxMouseEvent& event) {}
 void wxImagePanel::rightClick(wxMouseEvent& event) {}
 void wxImagePanel::mouseLeftWindow(wxMouseEvent& event) {}
 void wxImagePanel::keyPressed(wxKeyEvent& event) {}
 void wxImagePanel::keyReleased(wxKeyEvent& event) {}
 */

wxImagePanel::wxImagePanel(wxWindow* parent, const wxString& file, wxBitmapType format)
        : wxScrolledWindow(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize,
                  wxVSCROLL | wxHSCROLL | wxRETAINED /*| wxSUNKEN_BORDER | wxFULL_REPAINT_ON_RESIZE*/)
{
    // load the file... ideally add a check to see if loading was successful
    this->image.LoadFile(file, format);
    this->zoom = 0.25;
    this->offsetX = 0.0;
    this->offsetY = 0.0;
    this->ShowScrollbars(wxScrollbarVisibility::wxSHOW_SB_ALWAYS, wxScrollbarVisibility::wxSHOW_SB_ALWAYS);
    this->initialised = false;
    this->SetBackgroundStyle(wxBG_STYLE_PAINT);

    this->SetScrollbars(25, 25, this->image.GetWidth() / 25, this->image.GetHeight() / 25, 0, 0);
    this->AdjustScrollbars();

    Bind(wxEVT_LEFT_DOWN, &wxImagePanel::OnLeftDown, this);
    Bind(wxEVT_RIGHT_DOWN, &wxImagePanel::OnRightDown, this);
    Bind(wxEVT_SCROLLWIN_THUMBTRACK, &wxImagePanel::OnScroll, this);
    Bind(wxEVT_SCROLLWIN_THUMBRELEASE, &wxImagePanel::OnScroll, this);
    Bind(wxEVT_SCROLLWIN_LINEUP, &wxImagePanel::OnScroll, this);
    Bind(wxEVT_SCROLLWIN_LINEDOWN, &wxImagePanel::OnScroll, this);
}

/*
 * Called by the system of by wxWidgets when the panel needs
 * to be redrawn. You can also trigger this call by
 * calling Refresh()/Update().
 */

void wxImagePanel::paintEvent(wxPaintEvent& evt)
{
    // depending on your system you may need to look at double-buffered dcs
    wxAutoBufferedPaintDC dc(this);
    render(dc);
    evt.Skip();
}

/*
 * Alternatively, you can use a clientDC to paint on the panel
 * at any time. Using this generally does not free you from
 * catching paint events, since it is possible that e.g. the window
 * manager throws away your drawing when the window comes to the
 * background, and expects you will redraw it when the window comes
 * back (by sending a paint event).
 */
void wxImagePanel::paintNow()
{
    // depending on your system you may need to look at double-buffered dcs
    wxAutoBufferedPaintDC dc(this);
    render(dc);
}

/*
 * Here we do the actual rendering. I put it in a separate
 * method so that it can work no matter what type of DC
 * (e.g. wxPaintDC or wxClientDC) is used.
 */
void wxImagePanel::render(wxDC& dc)
{
    dc.SetBrush(*wxWHITE_BRUSH);
    dc.SetPen(*wxWHITE_PEN);
    dc.DrawRectangle(GetClientSize());
    PrepareDC(dc);
    if (!this->initialised) {
        dc.DrawBitmap(this->image, 0, 0, false);
        this->SetVirtualSize(this->image.GetSize());
        this->initialised = true;
    }
    if (this->image.IsOk()) {
        dc.SetUserScale(this->zoom, this->zoom);
        // the coordinates are affected by the userscale, too!
        dc.DrawBitmap(this->image, (this->offsetX / this->zoom), (this->offsetY / this->zoom), false);
    } else {
        dc.DrawText("Bitmap not OK", 10, 10);
    }
}

/*
 * Here we call refresh to tell the panel to draw itself again.
 * So when the user resizes the image panel the image should be resized too.
 */
void wxImagePanel::OnSize(wxSizeEvent& event)
{
    updateVirtualSize();
    event.Skip();
}

void wxImagePanel::AddZoom(double zoomFactor, const wxPoint& center)
{
    SetZoom(this->zoom * zoomFactor, center);
}

void wxImagePanel::SetZoom(double zoomFactor, const wxPoint& center)
{
    double image_x = ((double) center.x - this->offsetX) / this->zoom;
    double image_y = ((double) center.y - this->offsetY) / this->zoom;

    this->offsetX = center.x - image_x * zoomFactor;
    this->offsetY = center.y - image_y * zoomFactor;

    this->zoom = zoomFactor;
    AdjustScrollbars();
    Refresh();
}

void wxImagePanel::OnLeftDown(wxMouseEvent& evt)
{
    this->AddZoom(1.5, evt.GetPosition());
    evt.Skip();
}

void wxImagePanel::OnRightDown(wxMouseEvent& evt)
{
    this->AddZoom(0.5, evt.GetPosition());
    evt.Skip();
}

void wxImagePanel::updateVirtualSize()
{
    auto size = GetBestVirtualSize();
    SetVirtualSize(size);
    AdjustScrollbars();
}

void wxImagePanel::OnScroll(wxScrollWinEvent& evt)
{
    this->Scroll(GetScrollPos(wxHORIZONTAL), GetScrollPos(wxVERTICAL));
    evt.Skip(); // let the event go
}