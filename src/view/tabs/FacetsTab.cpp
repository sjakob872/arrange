#include "view/tabs/FacetsTab.h"

#include "view/GUIIdentifiers.h"
#include "view/dialogs/AddFacetDialog.h"
#include "view/interfaces/IArrangeController.h"
#include "view/lists/FacetsList.h"

#include <wx/file.h>
#include <wx/wx.h>

FacetsTab::FacetsTab(wxWindow* parent, wxWindowID winid, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
        : Tab(parent, winid, pos, size, style, name)
{
    this->controller = nullptr;
}

void FacetsTab::init(IArrangeController* c)
{
    this->controller = c;

    wxBoxSizer* facetsTabSizer = new wxBoxSizer(wxVERTICAL);
    this->SetSizer(facetsTabSizer);

    this->facetsListControl = new FacetsList(this, GUIIdentifiers::FACETS_LIST);

    this->facetsListControl->SetMinSize(wxSize(-1, -1));
    facetsTabSizer->Add(this->facetsListControl, 1, wxEXPAND | wxALL, 5);
    wxBoxSizer* facetsTabButtonSizer = new wxBoxSizer(wxTB_HORIZONTAL);
    this->addFacetsButton = new wxButton(this, GUIIdentifiers::ADD_FACET_BUTTON, "Add Facet");
    this->addFacetsButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &FacetsTab::OnAddFacetsButton, this);
    this->addFacetsButton->Disable();
    this->deleteFacetsButton = new wxButton(this, GUIIdentifiers::DELETE_FACET_BUTTON, "Delete Facet");
    this->deleteFacetsButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &FacetsTab::OnDeleteFacetsButton, this);
    this->deleteFacetsButton->Disable();
    this->saveFacetsButton = new wxButton(this, GUIIdentifiers::SAVE_FACETS_BUTTON, "Save Facets");
    this->saveFacetsButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &FacetsTab::OnSaveFacetsButton, this);
    this->saveFacetsButton->Disable();
    facetsTabButtonSizer->Add(this->addFacetsButton, 1, wxLEFT, 5);
    facetsTabButtonSizer->Add(this->deleteFacetsButton, 1, wxLEFT, 5);
    facetsTabButtonSizer->Add(this->saveFacetsButton, 1, wxLEFT, 5);
    facetsTabSizer->Add(facetsTabButtonSizer, 0, wxLEFT, 5);
}

void FacetsTab::enable()
{
    this->addFacetsButton->Enable();
    this->deleteFacetsButton->Enable();
    this->saveFacetsButton->Enable();
}

void FacetsTab::OnAddFacetsButton(wxCommandEvent& WXUNUSED(event))
{
    AddFacetDialog* dialog = new AddFacetDialog(this, wxID_ANY, "Add Facet");
    dialog->init(this->controller);
}

void FacetsTab::OnDeleteFacetsButton(wxCommandEvent& WXUNUSED(event))
{
    long itemIndex = -1;
    std::string selectedFacet = "";
    while ((itemIndex = this->facetsListControl->GetNextItem(itemIndex, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)) != wxNOT_FOUND) {
        // Got the selected item index
        selectedFacet = this->facetsListControl->GetItemText(itemIndex);
        this->controller->deleteFacet(selectedFacet);
        break;
    }
}

void FacetsTab::OnSaveFacetsButton(wxCommandEvent& WXUNUSED(event))
{
    wxFileDialog saveFileDialog(this, _("Save ASP Facets"), "~", "", "Lp files (*.lp)|*.lp", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
    if (saveFileDialog.ShowModal() == wxID_CANCEL) {
        return;
    }

    std::string filePath;
    if(saveFileDialog.GetPath().EndsWith(".lp")) {
        filePath = saveFileDialog.GetPath().SubString(0, saveFileDialog.GetPath().length() - 4);
    } else {
        filePath = saveFileDialog.GetPath();
    }
    wxFile file(filePath + ".lp", wxFile::write);
    if (!file.IsOpened()) {
        wxLogError("Cannot save current contents in file '%s'.", saveFileDialog.GetPath());
        return;
    }
    auto facets = this->controller->getFacetsASPProgram();
    file.Write(facets.first);
    file.Close();
    wxFile file2(filePath + "_rules.lp", wxFile::write);
    if (!file2.IsOpened()) {
        wxLogError("Cannot save current contents in file '%s'.", saveFileDialog.GetPath());
        return;
    }
    file2.Write(facets.second);
    file2.Close();
    saveFileDialog.Close();
    this->setDirty(false);
}

FacetsList* FacetsTab::getFacetsListControl() const
{
    return this->facetsListControl;
}
