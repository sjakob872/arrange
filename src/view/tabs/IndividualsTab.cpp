#include "view/tabs/IndividualsTab.h"

#include "view/GUIIdentifiers.h"
#include "view/dialogs/AddIndividualDialog.h"
#include "view/dialogs/EditIndividualDialog.h"
#include "view/interfaces/IArrangeController.h"
#include "view/lists/IndividualsList.h"

#include <wx/file.h>
#include <wx/wx.h>

IndividualsTab::IndividualsTab(wxWindow* parent, wxWindowID winid, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
        : Tab(parent, winid, pos, size, style, name)
{
}

void IndividualsTab::init(IArrangeController* controller)
{
    this->arrangeController = controller;

    wxBoxSizer* individualsTabSizer = new wxBoxSizer(wxVERTICAL);
    this->SetSizer(individualsTabSizer);

    this->individualsListControl = new IndividualsList(this, GUIIdentifiers::INDIVIDUALS_LIST);

    this->individualsListControl->SetMinSize(wxSize(200, -1));
    individualsTabSizer->Add(this->individualsListControl, 1, wxEXPAND | wxALL, 5);
    wxBoxSizer* individualsTabButtonSizer = new wxBoxSizer(wxTB_HORIZONTAL);
    this->addIndividualButton = new wxButton(this, GUIIdentifiers::ADD_INDIVIDUAL_BUTTON, "Add Individual");
    this->addIndividualButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &IndividualsTab::OnAddIndividualButton, this);
    this->addIndividualButton->Disable();
    this->editIndividualButton = new wxButton(this, GUIIdentifiers::EDIT_INDIVIDUAL_BUTTON, "Edit Individual");
    this->editIndividualButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &IndividualsTab::OnEditIndividualButton, this);
    this->editIndividualButton->Disable();
    this->deleteIndividualButton = new wxButton(this, GUIIdentifiers::DELETE_INDIVIDUAL_BUTTON, "Delete Individual");
    this->deleteIndividualButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &IndividualsTab::OnDeleteIndividualButton, this);
    this->deleteIndividualButton->Disable();
    this->saveIndividualsButton = new wxButton(this, GUIIdentifiers::SAVE_INDIVIDUALS_BUTTON, "Save Individuals");
    this->saveIndividualsButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &IndividualsTab::OnSaveIndividualsButton, this);
    this->saveIndividualsButton->Disable();
    individualsTabButtonSizer->Add(this->addIndividualButton, 1, wxLEFT, 5);
    individualsTabButtonSizer->Add(this->editIndividualButton, 1, wxLEFT, 5);
    individualsTabButtonSizer->Add(this->deleteIndividualButton, 1, wxLEFT, 5);
    individualsTabButtonSizer->Add(this->saveIndividualsButton, 1, wxLEFT, 5);
    individualsTabSizer->Add(individualsTabButtonSizer, 0, wxLEFT, 5);
}

void IndividualsTab::enable()
{
    this->addIndividualButton->Enable();
    this->editIndividualButton->Enable();
    this->deleteIndividualButton->Enable();
    this->saveIndividualsButton->Enable();
}

void IndividualsTab::OnDeleteIndividualButton(wxCommandEvent& WXUNUSED(event))
{
    long itemIndex = -1;
    std::string selectedIndividual = "";
    while ((itemIndex = this->individualsListControl->GetNextItem(itemIndex, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)) != wxNOT_FOUND) {
        // Got the selected item index
        selectedIndividual = this->individualsListControl->GetItemText(itemIndex);
        this->arrangeController->deleteIndividual(selectedIndividual);
        break;
    }
}

void IndividualsTab::OnAddIndividualButton(wxCommandEvent& WXUNUSED(event))
{
    AddIndividualDialog* dialog = new AddIndividualDialog(this, wxID_ANY, "Add Individual");
    dialog->init(this->arrangeController);
}

void IndividualsTab::OnSaveIndividualsButton(wxCommandEvent& WXUNUSED(event))
{
    wxFileDialog saveFileDialog(this, _("Save ASP Individuals"), "~", "", "Lp files (*.lp)|*.lp", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
    if (saveFileDialog.ShowModal() == wxID_CANCEL) {
        return;
    }

    std::string filePath;
    if(saveFileDialog.GetPath().EndsWith(".lp")) {
        filePath = saveFileDialog.GetPath();
    } else {
        filePath = saveFileDialog.GetPath() + ".lp";
    }
    wxFile file(filePath, wxFile::write);
    if (!file.IsOpened()) {
        wxLogError("Cannot save current contents in file '%s'.", saveFileDialog.GetPath());
        return;
    }
    file.Write(this->arrangeController->getIndividualsASPProgram());
    file.Close();
    saveFileDialog.Close();
    this->setDirty(false);
}

IndividualsList* IndividualsTab::getIndividualsListControl() const
{
    return this->individualsListControl;
}

void IndividualsTab::OnEditIndividualButton(wxCommandEvent& WXUNUSED(event))
{
    long itemIndex = -1;
    std::string selectedIndividual = "";
    std::string selectedIndividualConcept = "";
    while ((itemIndex = this->individualsListControl->GetNextItem(itemIndex, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)) != wxNOT_FOUND) {
        // Got the selected item index
        selectedIndividual = this->individualsListControl->GetItemText(itemIndex, 0);
        selectedIndividualConcept = this->individualsListControl->GetItemText(itemIndex, 1);
        break;
    }
    if (itemIndex == -1) {
        return;
    }
    EditIndividualDialog* dialog = new EditIndividualDialog(this, wxID_ANY, "Edit Individual");
    dialog->init(this->arrangeController, selectedIndividual, selectedIndividualConcept);
}
