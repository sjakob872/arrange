#include "view/tabs/OntologyProgramTab.h"

#include "view/GUIIdentifiers.h"

#include <wx/file.h>
#include <wx/wx.h>

OntologyProgramTab::OntologyProgramTab(
        wxWindow* parent, wxWindowID winid, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
        : Tab(parent, winid, pos, size, style, name)
{
}

void OntologyProgramTab::init()
{
    wxBoxSizer* ontologyProgramTabSizer = new wxBoxSizer(wxVERTICAL);
    this->SetSizer(ontologyProgramTabSizer);
    this->ontologyTextControl = new wxTextCtrl(
            this, GUIIdentifiers::ONTOLOGY_TEXTBOX, _T(""), wxPoint(0, 250), wxSize(100, 50), wxTE_MULTILINE | wxTE_READONLY);
    ontologyProgramTabSizer->Add(this->ontologyTextControl, 1, wxEXPAND | wxALL, 5);

    this->saveProgramButton = new wxButton(this, GUIIdentifiers::SAVE_RULES_BUTTON, "Save Ontology Program");
    this->saveProgramButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &OntologyProgramTab::OnSaveRulesButton, this);
    this->saveProgramButton->Disable();
    wxBoxSizer* buttonSizer = new wxBoxSizer(wxTB_HORIZONTAL);
    buttonSizer->Add(this->saveProgramButton, 0, wxLEFT, 5);
    ontologyProgramTabSizer->Add(buttonSizer, 0, wxEXPAND | wxALL, 5);
}

wxTextCtrl* OntologyProgramTab::getOntologyTextControl() const
{
    return this->ontologyTextControl;
}

void OntologyProgramTab::OnSaveRulesButton(wxCommandEvent& event)
{
    wxFileDialog saveFileDialog(this, _("Save Ontology Program"), "~", "", "Lp files (*.lp)|*.lp", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
    if (saveFileDialog.ShowModal() == wxID_CANCEL) {
        return;
    }

    wxFile file(saveFileDialog.GetPath() + ".lp", wxFile::write);
    if (!file.IsOpened()) {
        wxLogError("Cannot save current contents in file '%s'.", saveFileDialog.GetPath());
        return;
    }
    file.Write(this->ontologyTextControl->GetValue());
    file.Close();
    saveFileDialog.Close();
}

void OntologyProgramTab::enable()
{
    this->saveProgramButton->Enable();
}
