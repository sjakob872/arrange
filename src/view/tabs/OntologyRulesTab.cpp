#include "view/tabs/OntologyRulesTab.h"

#include "view/GUIIdentifiers.h"

#include <wx/file.h>
#include <wx/wx.h>

OntologyRulesTab::OntologyRulesTab(
        wxWindow* parent, wxWindowID winid, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
        : Tab(parent, winid, pos, size, style, name)
{
}

void OntologyRulesTab::init()
{
    wxBoxSizer* ontologyRuleTabSizer = new wxBoxSizer(wxVERTICAL);
    this->SetSizer(ontologyRuleTabSizer);
    this->ontologyRulesTextControl =
            new wxTextCtrl(this, GUIIdentifiers::RULE_TEXTBOX, _T(""), wxPoint(0, 250), wxSize(100, 50), wxTE_MULTILINE | wxTE_READONLY);
    ontologyRuleTabSizer->Add(this->ontologyRulesTextControl, 1, wxEXPAND | wxALL, 5);

    this->saveRulesButton = new wxButton(this, GUIIdentifiers::SAVE_RULES_BUTTON, "Save Ontology Rules");
    this->saveRulesButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &OntologyRulesTab::OnSaveRulesButton, this);
    this->saveRulesButton->Disable();
    wxBoxSizer* buttonSizer = new wxBoxSizer(wxTB_HORIZONTAL);
    buttonSizer->Add(this->saveRulesButton, 0, wxLEFT, 5);
    ontologyRuleTabSizer->Add(buttonSizer, 0, wxEXPAND | wxALL, 5);
}

wxTextCtrl* OntologyRulesTab::getOntologyRulesTextControl() const
{
    return this->ontologyRulesTextControl;
}

void OntologyRulesTab::OnSaveRulesButton(wxCommandEvent& event)
{
    wxFileDialog saveFileDialog(this, _("Save Ontology Rules"), "~", "", "Lp files (*.lp)|*.lp", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
    if (saveFileDialog.ShowModal() == wxID_CANCEL) {
        return;
    }

    wxFile file(saveFileDialog.GetPath() + ".lp", wxFile::write);
    if (!file.IsOpened()) {
        wxLogError("Cannot save current contents in file '%s'.", saveFileDialog.GetPath());
        return;
    }
    file.Write(this->ontologyRulesTextControl->GetValue());
    file.Close();
    saveFileDialog.Close();
}

void OntologyRulesTab::enable()
{
    this->saveRulesButton->Enable();
}
