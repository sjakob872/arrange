#include "view/tabs/Tab.h"
#include <iostream>
#include <wx/bookctrl.h>

Tab::Tab(wxWindow* parent, wxWindowID winid, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
        : wxPanel(parent, winid, pos, size, style, name)
{
    this->dirty = false;
}

bool Tab::isDirty() const
{
    return this->dirty;
}

void Tab::setDirty(bool dirtyFlag)
{
    int pos = ((wxBookCtrl*) this->m_parent)->FindPage(this);
    auto pageText = ((wxBookCtrl*) this->m_parent)->GetPageText(pos);
    if (dirtyFlag) {
        if (pageText.find('*') == std::string::npos) {
            ((wxBookCtrl*) this->m_parent)->SetPageText(pos, pageText + "*");
        }
    } else {
        ((wxBookCtrl*) this->m_parent)->SetPageText(pos, pageText.erase(pageText.size() - 1, 1));
    }
    this->dirty = dirtyFlag;
}
