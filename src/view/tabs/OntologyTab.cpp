#include "view/tabs/OntologyTab.h"

#include "view/GUIIdentifiers.h"
#include "view/dialogs/AddEdgeDialog.h"
#include "view/dialogs/EditEdgeDialog.h"
#include "view/interfaces/IArrangeController.h"
#include "view/lists/EdgesList.h"

#include <wx/file.h>
#include <wx/wx.h>

OntologyTab::OntologyTab(wxWindow* parent, wxWindowID winid, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
        : Tab(parent, winid, pos, size, style, name)
{
}

void OntologyTab::init(IArrangeController* controller)
{
    this->arrangeController = controller;

    wxBoxSizer* leftVerticalBoxSizer = new wxBoxSizer(wxVERTICAL);
    wxStaticText* conceptListLabel = new wxStaticText(this, wxID_ANY, _T("Ontology Concepts:"));
    leftVerticalBoxSizer->Add(conceptListLabel, wxSizerFlags().Border(wxTOP, 5).Left());
    this->conceptListbox = new wxListBox(
            this, GUIIdentifiers::CONCEPT_LIST, wxPoint(-1, -1), wxSize(-1, -1), 0, {}, wxLB_SORT | wxLB_SINGLE | wxLB_NEEDED_SB);
    this->conceptListbox->SetMinSize(wxSize(200, -1));
    leftVerticalBoxSizer->Add(this->conceptListbox, 1, wxALL | wxEXPAND);

    wxBoxSizer* rightVerticalBoxSizer = new wxBoxSizer(wxVERTICAL);
    wxStaticText* conceptPropertyLabel = new wxStaticText(this, wxID_ANY, _T("Edges connected to Concept:"));
    rightVerticalBoxSizer->Add(conceptPropertyLabel, wxSizerFlags().Border(wxTOP, 5).Left());

    this->edgesListControl = new EdgesList(this, GUIIdentifiers::EDGES_LIST);

    rightVerticalBoxSizer->Add(this->edgesListControl, 1, wxALL | wxEXPAND);

    wxBoxSizer* ontologyTabButtonSizer = new wxBoxSizer(wxTB_HORIZONTAL);
    this->addEdgeButton = new wxButton(this, GUIIdentifiers::ADD_EDGE_BUTTON, "Add Edge");
    this->addEdgeButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &OntologyTab::OnAddEdgeButton, this);
    this->addEdgeButton->Disable();
    this->editEdgeButton = new wxButton(this, GUIIdentifiers::EDIT_EDGE_BUTTON, "Edit Edge");
    this->editEdgeButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &OntologyTab::OnEditEdgeButton, this);
    this->editEdgeButton->Disable();
    this->deleteEdgeButton = new wxButton(this, GUIIdentifiers::DELETE_EDGE_BUTTON, "Delete Edge");
    this->deleteEdgeButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &OntologyTab::OnDeleteEdgeButton, this);
    this->deleteEdgeButton->Disable();
    this->saveEdgesButton = new wxButton(this, GUIIdentifiers::SAVE_EDGES_BUTTON, "Save Edges");
    this->saveEdgesButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &OntologyTab::OnSaveEdgesButton, this);
    this->saveEdgesButton->Disable();
    ontologyTabButtonSizer->Add(this->addEdgeButton, 1, wxLEFT, 5);
    ontologyTabButtonSizer->Add(this->editEdgeButton, 1, wxLEFT, 5);
    ontologyTabButtonSizer->Add(this->deleteEdgeButton, 1, wxLEFT, 5);
    ontologyTabButtonSizer->Add(this->saveEdgesButton, 1, wxLEFT, 5);

    wxFlexGridSizer* horizontalFlexGridSizer = new wxFlexGridSizer(2);
    horizontalFlexGridSizer->Add(leftVerticalBoxSizer, 1, wxALL | wxEXPAND, 5);
    horizontalFlexGridSizer->Add(rightVerticalBoxSizer, 1, wxALL | wxEXPAND, 5);
    horizontalFlexGridSizer->AddGrowableRow(0, 1);
    horizontalFlexGridSizer->AddGrowableCol(1, 1);
    wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(horizontalFlexGridSizer, 1, wxALL | wxEXPAND);
    sizer->Add(ontologyTabButtonSizer, 0, wxLeft);
    this->SetSizer(sizer);

    this->conceptListbox->Bind(wxEVT_LISTBOX_DCLICK, &OntologyTab::OnListBoxDoubleClick, this);
}

void OntologyTab::enable()
{
    this->addEdgeButton->Enable();
    this->editEdgeButton->Enable();
    this->deleteEdgeButton->Enable();
    this->saveEdgesButton->Enable();
}

wxListBox* OntologyTab::getConceptListbox() const
{
    return this->conceptListbox;
}

void OntologyTab::OnAddEdgeButton(wxCommandEvent& WXUNUSED(event))
{
    AddEdgeDialog* dialog = new AddEdgeDialog(this, wxID_ANY, "Add Edge");
    dialog->init(this->arrangeController);
}

void OntologyTab::OnEditEdgeButton(wxCommandEvent& WXUNUSED(event))
{
    long itemIndex = -1;
    std::string selectedStartConcept = "";
    std::string selectedRelation = "";
    std::string selectedEndConcept = "";
    std::string selectedWeight = "";
    std::string selectedId = "";
    while ((itemIndex = this->edgesListControl->GetNextItem(itemIndex, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)) != wxNOT_FOUND) {
        // Got the selected item index
        selectedStartConcept = this->edgesListControl->GetItemText(itemIndex, 0);
        selectedRelation = this->edgesListControl->GetItemText(itemIndex, 1);
        selectedEndConcept = this->edgesListControl->GetItemText(itemIndex, 2);
        selectedWeight = this->edgesListControl->GetItemText(itemIndex, 3);
        selectedId = this->edgesListControl->GetItemText(itemIndex, 4);
        break;
    }
    if (itemIndex == -1) {
        return;
    }
    EditEdgeDialog* dialog = new EditEdgeDialog(this, wxID_ANY, "Edit Edge");
    dialog->init(this->arrangeController, selectedStartConcept, selectedRelation, selectedEndConcept, stoi(selectedWeight), selectedId);
}

void OntologyTab::OnDeleteEdgeButton(wxCommandEvent& WXUNUSED(event))
{
    long itemIndex = -1;
    std::string selectedStartConcept = "";
    std::string selectedRelation = "";
    std::string selectedEndConcept = "";
    std::string selectedId = "";
    while ((itemIndex = this->edgesListControl->GetNextItem(itemIndex, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)) != wxNOT_FOUND) {
        // Got the selected item index
        selectedStartConcept = this->edgesListControl->GetItemText(itemIndex, 0);
        selectedRelation = this->edgesListControl->GetItemText(itemIndex, 1);
        selectedEndConcept = this->edgesListControl->GetItemText(itemIndex, 2);
        selectedId = this->edgesListControl->GetItemText(itemIndex, 4);
        this->arrangeController->deleteEdge(selectedStartConcept, selectedRelation, selectedEndConcept, std::stol(selectedId));
        break;
    }
}

void OntologyTab::OnSaveEdgesButton(wxCommandEvent& WXUNUSED(event))
{
    wxFileDialog saveFileDialog(this, _("Save ASP Ontology Edges"), "~", "", "Lp files (*.lp)|*.lp", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
    if (saveFileDialog.ShowModal() == wxID_CANCEL) {
        return;
    }
    std::string filePath;
    if(saveFileDialog.GetPath().EndsWith(".lp")) {
        filePath = saveFileDialog.GetPath();
    } else {
        filePath = saveFileDialog.GetPath() + ".lp";
    }
    wxFile file(filePath, wxFile::write);
    if (!file.IsOpened()) {
        wxLogError("Cannot save current contents in file '%s'.", saveFileDialog.GetPath());
        return;
    }
    file.Write(this->arrangeController->getOntologyProgram());
    file.Close();
    saveFileDialog.Close();
    this->setDirty(false);
}

EdgesList* OntologyTab::getEdgesListControl() const
{
    return this->edgesListControl;
}

void OntologyTab::OnListBoxDoubleClick(wxCommandEvent& event)
{
    auto edges = this->arrangeController->getConnectedEdges(event.GetString().ToStdString());
    this->edgesListControl->clearEdges();
    for (const auto& edge : edges) {
        this->edgesListControl->addEdge(edge);
    }
    this->edgesListControl->setSelectedConcept(event.GetString().ToStdString());
    this->edgesListControl->SetItemCount(edges.size());
}
