#include "view/tabs/SolutionTab.h"

#include "view/GUIIdentifiers.h"
//#include "view/panels/wxImagePanel.h"

#include <wx/wx.h>

SolutionTab::SolutionTab(wxWindow* parent, wxWindowID winid, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
        : Tab(parent, winid, pos, size, style, name)
{
    // this->solutionGraph = nullptr;
}

void SolutionTab::init()
{
    this->ontologyProgramTabSizer = new wxBoxSizer(wxVERTICAL);
    this->SetSizer(this->ontologyProgramTabSizer);
    this->solutionTextControl = new wxTextCtrl(
            this, GUIIdentifiers::ONTOLOGY_TEXTBOX, _T(""), wxPoint(0, 250), wxSize(1000, 50), wxTE_MULTILINE | wxTE_READONLY);
    wxFlexGridSizer* statsOntoSizer = new wxFlexGridSizer(8);
    statsOntoSizer->AddGrowableRow(0, 1);
    for(int i = 0; i < statsOntoSizer->GetCols(); i++) {
        statsOntoSizer->AddGrowableCol(i, i % 2);
    }
    wxStaticText* numberOfEdgesST = new wxStaticText(this, wxID_ANY, "Number of Edges:");
    this->numberOfEdgesLabel = new wxStaticText(this, wxID_ANY, "0");
    wxStaticText* numberOfConceptsST = new wxStaticText(this, wxID_ANY, "Number of Concepts:");
    this->numberOfConceptsLabel = new wxStaticText(this, wxID_ANY, "0");
    wxStaticText* numberOfFacetsST = new wxStaticText(this, wxID_ANY, "Number of Facets:");
    this->numberOfFacetsLabel = new wxStaticText(this, wxID_ANY, "0");
    wxStaticText* numberOfIndividualsST = new wxStaticText(this, wxID_ANY, "Number of Individuals:");
    this->numberOfIndividualsLabel = new wxStaticText(this, wxID_ANY, "0");
    statsOntoSizer->Add(numberOfEdgesST,1,  wxEXPAND);
    statsOntoSizer->Add(this->numberOfEdgesLabel, 0 , wxEXPAND);
    statsOntoSizer->Add(numberOfConceptsST,1,  wxEXPAND);
    statsOntoSizer->Add(this->numberOfConceptsLabel, 0 , wxEXPAND);
    statsOntoSizer->Add(numberOfFacetsST, 1 , wxEXPAND);
    statsOntoSizer->Add(this->numberOfFacetsLabel, 1 , wxEXPAND);
    statsOntoSizer->Add(numberOfIndividualsST, 1 , wxEXPAND);
    statsOntoSizer->Add(this->numberOfIndividualsLabel, 0 , wxEXPAND);

    wxFlexGridSizer* statsSolveSizer = new wxFlexGridSizer(12);
    statsSolveSizer->AddGrowableRow(0, 1);
    for(int i = 0; i < statsSolveSizer->GetCols(); i++) {
        statsSolveSizer->AddGrowableCol(i, i % 2);
    }
    wxStaticText* addingST = new wxStaticText(this, wxID_ANY, "Adding Duration:");
    this->addingLabel = new wxStaticText(this, wxID_ANY, "0 ms");
    wxStaticText* groundingST = new wxStaticText(this, wxID_ANY, "Grounding Duration:");
    this->groundingLabel = new wxStaticText(this, wxID_ANY, "0 ms");
    wxStaticText* externalST = new wxStaticText(this, wxID_ANY, "Externals Duration:");
    this->externalLabel = new wxStaticText(this, wxID_ANY, "0 ms");
    wxStaticText* solvingST = new wxStaticText(this, wxID_ANY, "Solving Duration:");
    this->solvingLabel = new wxStaticText(this, wxID_ANY, "0 ms");
    wxStaticText* sumST = new wxStaticText(this, wxID_ANY, "Sum:");
    this->sumLabel = new wxStaticText(this, wxID_ANY, "0 ms");
    wxStaticText* queryST = new wxStaticText(this, wxID_ANY, "Query Duration:");
    this->queryLabel = new wxStaticText(this, wxID_ANY, "0 ms");
    statsSolveSizer->Add(addingST,1,  wxEXPAND);
    statsSolveSizer->Add(this->addingLabel, 1 , wxEXPAND);
    statsSolveSizer->Add(groundingST,1,  wxEXPAND);
    statsSolveSizer->Add(this->groundingLabel, 1 , wxEXPAND);
    statsSolveSizer->Add(externalST,1,  wxEXPAND);
    statsSolveSizer->Add(this->externalLabel, 1 , wxEXPAND);
    statsSolveSizer->Add(solvingST,1,  wxEXPAND);
    statsSolveSizer->Add(this->solvingLabel, 1 , wxEXPAND);
    statsSolveSizer->Add(sumST,1,  wxEXPAND);
    statsSolveSizer->Add(this->sumLabel, 1 , wxEXPAND);
    statsSolveSizer->Add(queryST,1,  wxEXPAND);
    statsSolveSizer->Add(this->queryLabel, 1 , wxEXPAND);

    this->ontologyProgramTabSizer->Add(this->solutionTextControl, 1, wxEXPAND | wxALL, 5);
    this->ontologyProgramTabSizer->Add(statsOntoSizer, 0, wxEXPAND, 5);
    this->ontologyProgramTabSizer->Add(statsSolveSizer, 0, wxEXPAND, 5);
    wxInitAllImageHandlers();
}

wxTextCtrl* SolutionTab::getSolutionTextControl() const
{
    return this->solutionTextControl;
}

void SolutionTab::drawSolutionGraph(std::string path)
{
    std::cout << path << std::endl;
    path += "classification.pdf";
    // if (this->solutionGraph != nullptr) {
    //  this->ontologyProgramTabSizer->Remove(1);
    //}
    system(("evince " + path).c_str());
    // this->solutionGraph = new wxImagePanel(this, path, wxBITMAP_TYPE_JPEG);
    // this->ontologyProgramTabSizer->Add(this->solutionGraph, 1, wxEXPAND | wxALL, 5);
    // this->ontologyProgramTabSizer->Layout();
}

wxStaticText *SolutionTab::getNumberOfEdgesLabel() const {
    return this->numberOfEdgesLabel;
}

wxStaticText *SolutionTab::getNumberOfConceptsLabel() const {
    return this->numberOfConceptsLabel;
}


wxStaticText *SolutionTab::getNumberOfFacetsLabel() const {
    return this->numberOfFacetsLabel;
}

wxStaticText *SolutionTab::getNumberOfIndividualsLabel() const {
    return this->numberOfIndividualsLabel;
}

wxStaticText *SolutionTab::getAddingLabel() const {
    return this->addingLabel;
}

wxStaticText *SolutionTab::getGroundingLabel() const {
    return this->groundingLabel;
}

wxStaticText *SolutionTab::getExternalLabel() const {
    return this->externalLabel;
}

wxStaticText *SolutionTab::getSolvingLabel() const {
    return this->solvingLabel;
}

wxStaticText *SolutionTab::getSumLabel() const {
    return this->sumLabel;
}

wxStaticText *SolutionTab::getQueryLabel() const {
    return this->queryLabel;
}