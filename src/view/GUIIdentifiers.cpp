#include "view/GUIIdentifiers.h"

#include <wx/crt.h>

const int GUIIdentifiers::BOOK_CTRL = 100;
const int GUIIdentifiers::CONCEPT_LIST = 101;
const int GUIIdentifiers::ONTOLOGY_TEXTBOX = 102;
const int GUIIdentifiers::LOAD_ONTOLOGY_FILE = 103;
const int GUIIdentifiers::CREATE_ONTOLOGY = 104;
const int GUIIdentifiers::RULE_TEXTBOX = 105;
const int GUIIdentifiers::PROGRESS_BAR_TIMER = 106;
const int GUIIdentifiers::LOAD_FACET_FILE = 108;
const int GUIIdentifiers::LOAD_INDIVIDUALS_FILE = 109;
const int GUIIdentifiers::SOLVE = 110;
const int GUIIdentifiers::INDIVIDUALS_LIST = 111;
const int GUIIdentifiers::ADD_INDIVIDUAL_BUTTON = 112;
const int GUIIdentifiers::EDIT_INDIVIDUAL_BUTTON = 113;
const int GUIIdentifiers::DELETE_INDIVIDUAL_BUTTON = 114;
const int GUIIdentifiers::SAVE_INDIVIDUALS_BUTTON = 115;
const int GUIIdentifiers::NEW_ONTOLOGY = 117;
const int GUIIdentifiers::ADD_EDGE_BUTTON = 118;
const int GUIIdentifiers::EDIT_EDGE_BUTTON = 119;
const int GUIIdentifiers::DELETE_EDGE_BUTTON = 120;
const int GUIIdentifiers::SAVE_EDGES_BUTTON = 121;
const int GUIIdentifiers::EDGES_LIST = 122;
const int GUIIdentifiers::SAVE_RULES_BUTTON = 123;
const int GUIIdentifiers::ADD_FACET_BUTTON = 124;
const int GUIIdentifiers::DELETE_FACET_BUTTON = 126;
const int GUIIdentifiers::SAVE_FACETS_BUTTON = 127;
const int GUIIdentifiers::FACETS_LIST = 128;
const int GUIIdentifiers::SETTINGS = 129;
const int GUIIdentifiers::FILE_QUIT = wxID_EXIT;
const int GUIIdentifiers::HELP_ABOUT = wxID_ABOUT;