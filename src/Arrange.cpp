#include "Arrange.h"

#include "controller/MainController.h"
#include "controller/SolveController.h"
#include "model/OntologyManager.h"
#include "view/GUIIdentifiers.h"
#include "view/MainFrame.h"
#include "view/panels/wxImagePanel.h"
#include "view/tabs/IndividualsTab.h"
#include "view/tabs/OntologyTab.h"

bool Arrange::OnInit()
{
    this->frame = new MainFrame(_T("Arrange - Answer pRogRAmming oNtoloGy crEator"));
    this->controller = new MainController();
    this->solveController = new SolveController();
    this->frame->init(this->controller, this->solveController);
    this->ontologyManager = new OntologyManager();
    this->ontologyManager->setController(this->controller);
    this->controller->setOntologyManager(this->ontologyManager);
    this->controller->setMainFrame(this->frame);
    this->solveController->setOntologyManager(this->ontologyManager);
    this->solveController->setMainFrame(this->frame);
    this->frame->Maximize(true);
    this->frame->Show(true);
    return true;
}

void Arrange::CleanUp()
{
    delete this->controller;
    delete this->solveController;
    delete this->ontologyManager;
}

// Attach the event handlers. Put this after MainFrame declaration.
BEGIN_EVENT_TABLE(MainFrame, wxFrame)
EVT_MENU(GUIIdentifiers::FILE_QUIT, MainFrame::OnQuit)
EVT_MENU(GUIIdentifiers::HELP_ABOUT, MainFrame::OnAbout)
EVT_MENU(GUIIdentifiers::LOAD_ONTOLOGY_FILE, MainFrame::OnLoadOntology)
EVT_MENU(GUIIdentifiers::LOAD_FACET_FILE, MainFrame::OnLoadFacets)
EVT_MENU(GUIIdentifiers::LOAD_INDIVIDUALS_FILE, MainFrame::OnLoadIndividuals)
EVT_MENU(GUIIdentifiers::CREATE_ONTOLOGY, MainFrame::OnCreateOntology)
EVT_MENU(GUIIdentifiers::SOLVE, MainFrame::OnSolve)
EVT_MENU(GUIIdentifiers::SETTINGS, MainFrame::OnSettings)
EVT_MENU(GUIIdentifiers::NEW_ONTOLOGY, MainFrame::OnNewOntology)
EVT_TIMER(GUIIdentifiers::PROGRESS_BAR_TIMER, MainFrame::OnTimerEvent)
END_EVENT_TABLE()

IMPLEMENT_APP(Arrange)