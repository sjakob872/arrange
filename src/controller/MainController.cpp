#include "controller/MainController.h"

#include "model/FacetTypes.h"
#include "model/Individual.h"
#include "model/OntologyManager.h"
#include "view/MainFrame.h"
#include "view/lists/EdgesList.h"
#include "view/lists/FacetsList.h"
#include "view/lists/IndividualsList.h"
#include "view/tabs/FacetsTab.h"
#include "view/tabs/IndividualsTab.h"
#include "view/tabs/OntologyTab.h"
#include "view/tabs/SolutionTab.h"

#include <AnswerGraph.h>
#include <Ontology.h>
#include <OntologyExtractor.h>
#include <asp/OntologyTerms.h>
#include <conceptnet/Concept.h>
#include <conceptnet/Edge.h>

MainController::MainController()
{
    this->ontologyManager = nullptr;
    this->mainframe = nullptr;
}

MainController::~MainController() = default;

void MainController::createCN5Ontology(std::string& rootConcept)
{
    if (this->ontologyManager == nullptr) {
        return;
    }
    this->ontologyManager->createCN5Ontology(rootConcept);
}

void MainController::createIndividual(std::string individual, bool loaded)
{
    if (this->ontologyManager == nullptr) {
        return;
    }
    this->ontologyManager->createIndividual(individual, loaded);
}

void MainController::setOntologyManager(OntologyManager* ontologyManager)
{
    this->ontologyManager = ontologyManager;
}

void MainController::setMainFrame(MainFrame* mainFrame)
{
    this->mainframe = mainFrame;
}

void MainController::ontologyCreated()
{
    this->mainframe->ontologyCreationFinished();
    this->generateConceptList();
    this->mainframe->getSolutionTab()->getNumberOfConceptsLabel()->SetLabel(std::to_string(this->ontologyManager->getOntologyExtractor()->getAnswerGraph()->getConcepts().size()));
    this->mainframe->getSolutionTab()->getNumberOfEdgesLabel()->SetLabel(std::to_string(this->ontologyManager->getOntologyExtractor()->getAnswerGraph()->getEdges().size()));
}

void MainController::generateConceptList()
{
    this->mainframe->getConceptListbox()->Clear();
    for (const auto& con : this->ontologyManager->getOntologyExtractor()->getAnswerGraph()->getConcepts()) {
        if (con.second->term.empty()) {
            continue;
        }
        this->mainframe->getConceptListbox()->Append(wxString(con.second->term));
    }
    this->mainframe->getOntologyTextControl()->SetValue(wxString(this->ontologyManager->getOntologyExtractor()->getOntology()->ontology));
    this->mainframe->getOntologyRulesTextControl()->SetValue(wxString(this->ontologyManager->getOntologyExtractor()->getOntology()->rules));
    this->mainframe->getSolutionTab()->getNumberOfConceptsLabel()->SetLabel(std::to_string(this->ontologyManager->getOntologyExtractor()->getAnswerGraph()->getConcepts().size()));
    this->mainframe->getSolutionTab()->getNumberOfEdgesLabel()->SetLabel(std::to_string(this->ontologyManager->getOntologyExtractor()->getAnswerGraph()->getEdges().size()));

}

void MainController::individualCreated(Individual* individual, bool loaded, int individualsCount)
{
    this->mainframe->getIndividualsTab()->getIndividualsListControl()->addIndividual(
            individual->getName(), individual->getBaseConceptName());
    if (!loaded) {
        this->mainframe->getIndividualsTab()->setDirty(true);
    }
    this->mainframe->getSolutionTab()->getNumberOfIndividualsLabel()->SetLabel(std::to_string(individualsCount));
}

void MainController::createIndividual(std::string individualName, std::string baseConcept)
{
    this->ontologyManager->createIndividual(individualName, baseConcept, false);
}

std::vector<std::string> MainController::getAvailableConcepts()
{
    std::vector<std::string> ret;
    if (this->ontologyManager->getOntologyExtractor() != nullptr &&
            this->ontologyManager->getOntologyExtractor()->getAnswerGraph() != nullptr) {
        for (const auto& pair : this->ontologyManager->getOntologyExtractor()->getAnswerGraph()->getConcepts()) {
            ret.push_back(pair.second->term);
        }
    } else {
        ret.emplace_back("test1");
        ret.emplace_back("test2");
    }
    return ret;
}

std::string MainController::getIndividualsASPProgram()
{
    std::string ret = "";
    for (const auto& pair : this->ontologyManager->getIndividuals()) {
        ret.append(asp::OntologyTerms::INPUT + "(\"" + pair.second->getName() + "\", \"" + pair.second->getBaseConceptName() + "\").\n");
    }
    return ret;
}

std::string MainController::getOntologyProgram()
{
    return this->ontologyManager->getOntologyExtractor()->getOntology()->ontology;
}

void MainController::deleteIndividual(std::string individual)
{
    this->ontologyManager->deleteIndividual(individual);
}

void MainController::individualDeleted(std::string individual, int individualsCount)
{
    this->mainframe->getIndividualsTab()->getIndividualsListControl()->deleteIndividual(individual);
    this->mainframe->getSolutionTab()->getNumberOfIndividualsLabel()->SetLabel(std::to_string(individualsCount));
}

void MainController::editIndividual(std::string individualOldName, std::string individualNewName, std::string baseConcept)
{
    this->ontologyManager->editIndividual(individualOldName, individualNewName, baseConcept);
}

void MainController::individualEdited(std::string individualOldName, Individual* individual)
{
    this->mainframe->getIndividualsTab()->getIndividualsListControl()->editIndividual(
            individualOldName, individual->getName(), individual->getBaseConceptName());
}

void MainController::loadOntology(std::string ontology)
{
    this->ontologyManager->loadOntology(ontology);
}

void MainController::createNewOntology()
{
    this->ontologyManager->createNewOntology();
}

std::vector<std::string> MainController::getOntologyRelations()
{
    return this->ontologyManager->getOntologyExtractor()->getOntologyRelations();
}

void MainController::createEdge(std::string fromConcept, std::string relation, std::string toConcept, double weight)
{
    this->ontologyManager->createEdge(fromConcept, relation, toConcept, weight);
}

void MainController::edgeCreated(std::string fromConcept, std::string relation, std::string toConcept, double weight, long id)
{
    this->generateConceptList();
    std::string selectedConcept = this->mainframe->getOntologyTab()->getEdgesListControl()->getSelectedConcept();
    if (selectedConcept == fromConcept || selectedConcept == toConcept) {
        this->mainframe->getOntologyTab()->getEdgesListControl()->addEdge(fromConcept, relation, toConcept, weight, id);
    }

    this->mainframe->getSolutionTab()->getNumberOfConceptsLabel()->SetLabel(std::to_string(this->ontologyManager->getOntologyExtractor()->getAnswerGraph()->getConcepts().size()));
    this->mainframe->getSolutionTab()->getNumberOfEdgesLabel()->SetLabel(std::to_string(this->ontologyManager->getOntologyExtractor()->getAnswerGraph()->getEdges().size()));
}

void MainController::editEdge(std::string oldStartConcept, std::string newStartConcept, std::string oldRelation, std::string newRelation,
        std::string oldEndConcept, std::string newEndConcept, double weight, long id)
{
    this->ontologyManager->editEdge(oldStartConcept, newStartConcept, oldRelation, newRelation, oldEndConcept, newEndConcept, weight, id);
}

void MainController::deleteEdge(std::string startConcept, std::string relation, std::string endConcept, long id)
{
    this->ontologyManager->deleteEdge(startConcept, relation, endConcept, id);
}

void MainController::edgeEdited(std::string oldStartConcept, std::string oldRelation, std::string oldEndConcept,
        std::string newStartConcept, std::string newRelation, std::string newEndConcept, double weight, long id)
{
    this->mainframe->getOntologyTab()->getEdgesListControl()->editEdge(
            oldStartConcept, newStartConcept, oldRelation, newRelation, oldEndConcept, newEndConcept, weight, id);
    this->mainframe->getOntologyTextControl()->SetValue(wxString(this->ontologyManager->getOntologyExtractor()->getOntology()->ontology));
    this->mainframe->getOntologyRulesTextControl()->SetValue(wxString(this->ontologyManager->getOntologyExtractor()->getOntology()->rules));
    this->mainframe->getOntologyTab()->setDirty(true);
}

void MainController::edgeDeleted(std::string startConcept, std::string relation, std::string endConcept, long id)
{
    this->mainframe->getOntologyTab()->getEdgesListControl()->deleteEdge(startConcept, relation, endConcept, id);
    this->mainframe->getOntologyTextControl()->SetValue(wxString(this->ontologyManager->getOntologyExtractor()->getOntology()->ontology));
    this->mainframe->getOntologyRulesTextControl()->SetValue(wxString(this->ontologyManager->getOntologyExtractor()->getOntology()->rules));

    this->mainframe->getOntologyTab()->setDirty(true);
    this->mainframe->getSolutionTab()->getNumberOfConceptsLabel()->SetLabel(std::to_string(this->ontologyManager->getOntologyExtractor()->getAnswerGraph()->getConcepts().size()));
    this->mainframe->getSolutionTab()->getNumberOfEdgesLabel()->SetLabel(std::to_string(this->ontologyManager->getOntologyExtractor()->getAnswerGraph()->getEdges().size()));
}

std::vector<std::tuple<std::string, std::string, std::string, double, long>> MainController::getConnectedEdges(std::string conceptName)
{
    return this->ontologyManager->getConnectedEdges(conceptName);
}

void MainController::createFacets(std::vector<std::string> facets, bool translateSupplementary)
{
    this->ontologyManager->createFacets(facets, translateSupplementary);
}

void MainController::facetsCreated(std::vector<std::string> facets, bool dirty, int facetsCount)
{
    this->mainframe->getFacetsTab()->getFacetsListControl()->addFacets(facets);
    this->mainframe->getFacetsTab()->setDirty(dirty);
    this->mainframe->getSolutionTab()->getNumberOfFacetsLabel()->SetLabel(std::to_string(facetsCount));
}

std::pair<std::string,std::string> MainController::getFacetsASPProgram()
{
    return this->ontologyManager->getFacetsProgram();
}

void MainController::deleteFacet(std::string facet)
{
    this->ontologyManager->deleteFacet(facet);
}

void MainController::facetDeleted(std::string facet, int facetsCount)
{
    this->mainframe->getFacetsTab()->getFacetsListControl()->deleteFacet(facet);
    this->mainframe->getFacetsTab()->setDirty(true);
    this->mainframe->getSolutionTab()->getNumberOfFacetsLabel()->SetLabel(std::to_string(facetsCount));
}

std::vector<std::string> MainController::getFacetRelations(bool skip)
{
    std::vector<std::string> ret;
    for (auto rel : facetTypes) {
        if (rel == facetTypes[FacetType::propertyViolation] && skip) {
            continue;
        }
        ret.emplace_back(rel);
    }
    return ret;
}

std::vector<std::string> MainController::getAvailableIndividuals()
{
    std::vector<std::string> ret;
    for (const auto& individual : this->ontologyManager->getIndividuals()) {
        ret.emplace_back(individual.second->getName());
    }
    return ret;
}

void MainController::createFacet(std::string fromConcept, std::string relation, std::string toConcept, std::string typeString,
        std::string valueRange, int minCard, int maxCard, std::string individual, std::string value, long timeStep)
{
    this->ontologyManager->createFacet(
            fromConcept, relation, toConcept, typeString, valueRange, minCard, maxCard, individual, value, timeStep);
}

void MainController::conceptCreated()
{
    this->generateConceptList();
}

void MainController::applySettings(
        double isAWeight, double formOfWeight, double synonymWeight, double hasPropertyWeight, double minRelatedness)
{
    this->ontologyManager->getOntologyExtractor()->setIsAWeight(isAWeight);
    this->ontologyManager->getOntologyExtractor()->setFormOfWeight(formOfWeight);
    this->ontologyManager->getOntologyExtractor()->setSynonymWeight(synonymWeight);
    this->ontologyManager->getOntologyExtractor()->setHasPropertyWeight(hasPropertyWeight);
    this->ontologyManager->getOntologyExtractor()->setMinRelatedness(minRelatedness);
}
