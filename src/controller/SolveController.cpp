#include "controller/SolveController.h"

#include "model/OntologyManager.h"
#include "view/MainFrame.h"

#include <OntologyExtractor.h>

#include <iostream>

SolveController::SolveController()
{
    this->mainFrame = nullptr;
    this->ontologyManager = nullptr;
    this->thread = nullptr;
}

SolveController::~SolveController()
{
    if (this->thread != nullptr) {
        this->thread->join();
        delete this->thread;
    }
}

void SolveController::setOntologyManager(OntologyManager* manager)
{
    this->ontologyManager = manager;
}

void SolveController::setMainFrame(MainFrame* mainframe)
{
    this->mainFrame = mainframe;
}

void SolveController::solve()
{
    std::vector<std::string> solutions = this->ontologyManager->solve(this->mainFrame->getResourcesPath());
    for (const std::string& solution : solutions) {
        this->mainFrame->getSolutionTab()->getSolutionTextControl()->AppendText(solution + "\n");
    }
    if (this->thread != nullptr) {
        this->thread->join();
        delete this->thread;
    }
    this->thread = new std::thread(&SolutionTab::drawSolutionGraph, this->mainFrame->getResourcesPath());
}

std::vector<int> SolveController::getStatistics() {
    return this->ontologyManager->getOntologyExtractor()->getStatistics();
}
