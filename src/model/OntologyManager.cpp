#include "model/OntologyManager.h"

#include "model/Facet.h"
#include "model/FacetManager.h"
#include "model/Individual.h"
#include "model/interfaces/IOntologyController.h"

#include "conceptnet/Edge.h"
#include <AnswerGraph.h>
#include <OntologyExtractor.h>
#include <asp/OntologyTerms.h>
#include <conceptnet/Concept.h>
#include <iostream>
#include <thread>
#include <utility>

OntologyManager::OntologyManager()
{
    this->ontologyExtractor = nullptr;
    this->checkThread = nullptr;
    this->extractionThread = nullptr;
    this->controller = nullptr;
    this->facetManager = new FacetManager(this);
}

OntologyManager::~OntologyManager()
{
    cleanUp();

    delete this->facetManager;

    if (this->extractionThread) {
        this->extractionThread->join();
        delete this->extractionThread;
    }
    if (this->checkThread) {
        this->checkThread->join();
        delete this->checkThread;
    }
}

void OntologyManager::finished()
{
    while (true) {
        std::unique_lock<std::mutex> lock(this->mtx);
        if (this->cv.wait_for(lock, std::chrono::seconds(1)) == std::cv_status::timeout) {
            if (this->ontologyExtractor->isFinishedExtracting()) {
                break;
            }
        }
    }
    this->controller->ontologyCreated();
}

void OntologyManager::createIndividual(const std::string& individual, bool loaded)
{
    // is("abc", "bombs").
    std::size_t posIndividualStart = individual.find_first_of('"');
    std::size_t posIndividualEnd = individual.find('"', posIndividualStart + 1);
    std::size_t posConceptStart = individual.find('"', posIndividualEnd + 1);
    std::size_t posConceptEnd = individual.find_last_of('"');
    std::string name = individual.substr(posIndividualStart + 1, posIndividualEnd - posIndividualStart - 1);
    std::string conceptName = individual.substr(posConceptStart + 1, posConceptEnd - posConceptStart - 1);
    this->createIndividual(name, conceptName, loaded);
}

void OntologyManager::createIndividual(const std::string& individualName, const std::string& conceptName, bool loaded)
{
    conceptnet::Concept* baseConcept = getConcept(conceptName);
    Individual* i = new Individual(individualName, conceptName, baseConcept);
    this->individuals.emplace(individualName, i);

    this->controller->individualCreated(i, loaded, this->individuals.size());
}

conceptnet::Concept* OntologyManager::getConcept(const std::string& conceptName) const
{
    conceptnet::Concept* baseConcept = nullptr;
    if (ontologyExtractor != nullptr && ontologyExtractor->getAnswerGraph() != nullptr) {
        baseConcept = ontologyExtractor->getAnswerGraph()->getConcept("/c/en/" + conceptName);
    }
    return baseConcept;
}

void OntologyManager::createCN5Ontology(std::string& rootConcept)
{
    cleanUp();
    this->ontologyExtractor = new cnoe::OntologyExtractor();
    this->extractionThread = new std::thread(&cnoe::OntologyExtractor::extract, this->ontologyExtractor, rootConcept);
    this->checkThread = new std::thread(&OntologyManager::finished, this);
}

void OntologyManager::cleanUp() const
{
    if (this->ontologyExtractor != nullptr) {
        delete ontologyExtractor;
    }
    for (const auto& pair : individuals) {
        delete pair.second;
    }
    this->facetManager->cleanUp();
}

void OntologyManager::setController(IOntologyController* ontologyController)
{
    this->controller = ontologyController;
}

cnoe::OntologyExtractor* OntologyManager::getOntologyExtractor() const
{
    return this->ontologyExtractor;
}

const std::map<std::string, Individual*>& OntologyManager::getIndividuals() const
{
    return this->individuals;
}

void OntologyManager::deleteIndividual(const std::string& individualName)
{
    if (this->individuals.find(individualName) == this->individuals.end()) {
        return;
    }
    Individual* individual = this->individuals.at(individualName);
    this->individuals.erase(individualName);
    delete individual;
    this->controller->individualDeleted(individualName, this->individuals.size());
}

void OntologyManager::editIndividual(
        const std::string& individualOldName, const std::string& individualNewName, const std::string& baseConcept)
{
    if (this->individuals.find(individualOldName) == this->individuals.end()) {
        return;
    }
    Individual* individual = this->individuals.at(individualOldName);
    individual->name = individualNewName;
    individual->baseConceptName = baseConcept;
    individual->baseConcept = getConcept(baseConcept);
    if (individualOldName != individualNewName) {
        this->individuals.erase(individualOldName);
        this->individuals.emplace(individualNewName, individual);
    } else {
        this->individuals[individualOldName] = individual;
    }
    this->controller->individualEdited(individualOldName, individual);
}

void OntologyManager::loadOntology(const std::string& ontology)
{
    cleanUp();
    this->ontologyExtractor = new cnoe::OntologyExtractor();
    this->extractionThread = new std::thread(&cnoe::OntologyExtractor::extractOntologyFromString, this->ontologyExtractor, ontology);
    this->checkThread = new std::thread(&OntologyManager::finished, this);
}

std::vector<std::string> OntologyManager::solve(std::string solutionPath)
{
    std::string individualsProgram = "";
    for (const auto& pair : this->individuals) {
        individualsProgram.append(asp::OntologyTerms::INPUT + "(\"" + pair.second->name + "\", \"" + pair.second->baseConceptName + "\").\n");
    }
    return this->ontologyExtractor->solve(individualsProgram, this->facetManager->translateFacets(), std::move(solutionPath));
}

void OntologyManager::createNewOntology()
{
    cleanUp();
    this->ontologyExtractor = new cnoe::OntologyExtractor();
    this->ontologyExtractor->createEmptyOntology();
    this->controller->ontologyCreated();
}

void OntologyManager::createEdge(const std::string& fromConcept, const std::string& relation, const std::string& toConcept, double weight)
{
    long id = this->ontologyExtractor->addEdge(fromConcept, relation, toConcept, weight);
    this->controller->edgeCreated(fromConcept, relation, toConcept, weight, id);
}

void OntologyManager::deleteEdge(const std::string& fromConcept, const std::string& relation, const std::string& toConcept, long id)
{
    this->ontologyExtractor->deleteEdge(fromConcept, relation, toConcept, id);
    this->controller->edgeDeleted(fromConcept, relation, toConcept, id);
}

void OntologyManager::editEdge(const std::string& oldStartConcept, const std::string& newStartConcept, const std::string& oldRelation,
        const std::string& newRelation, const std::string& oldEndConcept, const std::string& newEndConcept, double weight, long id)
{
    this->ontologyExtractor->editEdge(oldStartConcept, newStartConcept, oldRelation, newRelation, oldEndConcept, newEndConcept, weight, id);
    this->controller->edgeEdited(oldStartConcept, oldRelation, oldEndConcept, newStartConcept, newRelation, newEndConcept, weight, id);
}

std::vector<std::tuple<std::string, std::string, std::string, double, long>> OntologyManager::getConnectedEdges(std::string conceptName)
{
    std::vector<std::tuple<std::string, std::string, std::string, double, long>> ret;
    auto edges = this->ontologyExtractor->getAnswerGraph()->getConcept(std::move(conceptName))->getEdges();
    ret.reserve(edges.size());
    for (auto edge : edges) {
        ret.emplace_back(edge->fromConcept->term, conceptnet::relations[edge->relation], edge->toConcept->term, edge->weight, edge->aspId);
    }
    return ret;
}

std::vector<std::string> OntologyManager::getFacets(bool translateSupplementary) const
{
    return this->facetManager->getFacets(translateSupplementary);
}

void OntologyManager::createFacets(const std::vector<std::string>& facets, bool translateSupplementary)
{
    for (const auto& facet : facets) {
        this->facetManager->createFacet(facet);
    }
    this->controller->facetsCreated(this->facetManager->getFacets(translateSupplementary), false, this->facetManager->getNumberOfFacets());
}

std::pair<std::string,std::string> OntologyManager::getFacetsProgram() const {
    return this->facetManager->translateFacets();
}

void OntologyManager::deleteFacet(const std::string& facet) {
    this->facetManager->deleteFacet(facet);
    this->controller->facetDeleted(facet, this->facetManager->getNumberOfFacets());
}

void OntologyManager::createFacet(const std::string& fromConcept, const std::string& relation, const std::string& toConcept,
                                  const std::string& typeString, const std::string& valueRange, int minCard, int maxCard,
                                  const std::string& individual, const std::string& value, long timeStep) {
    this->facetManager->createFacet(fromConcept, relation, toConcept, typeString, valueRange, minCard, maxCard, individual, value, timeStep);
    this->controller->facetsCreated(this->facetManager->getFacets(false), true, this->facetManager->getNumberOfFacets());
}

void OntologyManager::conceptCreated() {
    this->controller->conceptCreated();
}
