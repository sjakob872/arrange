#include "model/Facet.h"

#include <iostream>
#include <sstream>
#include <utility>

Facet::Facet(std::string firstConceptName, const std::string& type, std::string secondConceptName)
        : firstConceptName(std::move(firstConceptName))
        , secondConceptName(std::move(secondConceptName))
{
    this->type = getFacetType(type);
    this->maxCardinality = -1;
    this->minCardinality = -1;
    this->timeStep = -1;
}

[[maybe_unused]] const std::string& Facet::getTypeString() const
{
    return this->typeString;
}

void Facet::setTypeString(const std::string& tString)
{
    this->typeString = tString;
}

const std::string& Facet::getValueRange() const
{
    return this->valueRange;
}

void Facet::setValueRange(const std::string& range)
{
    this->valueRange = range;
}

const std::string& Facet::getIndividualName() const
{
    return this->individualName;
}

void Facet::setIndividualName(const std::string& individual)
{
    this->individualName = individual;
}

const std::string& Facet::getValue() const
{
    return this->value;
}

void Facet::setValue(const std::string& val)
{
    this->value = val;
}

const std::vector<std::string>& Facet::getSupplementaryRules() const
{
    return this->supplementaryRules;
}

void Facet::setSupplementaryRule(const std::string& supplementaryRule)
{
    this->supplementaryRules.emplace_back(supplementaryRule);
}

int Facet::getMinCardinality() const
{
    return this->minCardinality;
}

void Facet::setMinCardinality(int min)
{
    this->minCardinality = min;
}

int Facet::getMaxCardinality() const
{
    return this->maxCardinality;
}

void Facet::setMaxCardinality(int max)
{
    this->maxCardinality = max;
}

long Facet::getTimeStep() const
{
    return this->timeStep;
}

void Facet::setTimeStep(long t)
{
    this->timeStep = t;
}

FacetType Facet::getFacetType(const std::string& facetType)
{
    int position = 0;
    for (auto& rel : facetTypes) {
        std::string str(rel);
        if (str == facetType) {
            return static_cast<FacetType>(position);
        }
        position++;
    }
}

std::string Facet::trim(const std::string& str)
{
    const std::string& whitespace = " \t";
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos) {
        return ""; // no content
    }
    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

std::string Facet::toString()
{
    std::stringstream stream;
    stream << "first: " << this->firstConceptName << " type: " << facetTypes[this->type] << " second: " << this->secondConceptName
           << " min: " << this->minCardinality << " max: " << this->maxCardinality << " typeString: " << this->typeString
           << " valueRange: " << this->valueRange << " value: " << this->value << " individualName: " << this->individualName
           << " timeStep: " << this->timeStep << "supplementary rules: ";
    for (const std::string& rule : this->supplementaryRules) {
        stream << rule;
    }
    return stream.str();
}
