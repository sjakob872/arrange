#include "model/FacetManager.h"

#include "model/Facet.h"
#include "model/OntologyManager.h"

#include <Ontology.h>
#include <OntologyExtractor.h>

#include <algorithm>
#include <iostream>
#include <sstream>

FacetManager::FacetManager(OntologyManager* manager)
{
    this->ontologyManager = manager;
    this->facets.emplace(FacetType::facetOf, std::vector<Facet*>());
    this->facets.emplace(FacetType::typeOf, std::vector<Facet*>());
    this->facets.emplace(FacetType::valueRangeOf, std::vector<Facet*>());
    this->facets.emplace(FacetType::hasValue, std::vector<Facet*>());
    this->facets.emplace(FacetType::subPropertyOf, std::vector<Facet*>());
    this->facets.emplace(FacetType::domainOf, std::vector<Facet*>());
}

FacetManager::~FacetManager()
{
    cleanUp();
}

void FacetManager::cleanUp()
{
    for (const auto& facetPair : this->facets) {
        for (auto facet : facetPair.second) {
            delete facet;
        }
    }
    this->facets.clear();
}

void FacetManager::createFacet(const std::string& facet)
{
    // #external facetOf("threatLevel", "dangerous", 24).
    if (facet.find("#external") != std::string::npos) {
        std::size_t posFacetTypeStart = facet.find_first_of(' ');
        std::size_t posFacetTypeEnd = facet.find('(', posFacetTypeStart + 1);
        std::string facetType = facet.substr(posFacetTypeStart + 1, posFacetTypeEnd - posFacetTypeStart - 1);
        if (facetType == facetTypes[FacetType::facetOf]) {
            getOrCreateFacetOfFacet(facet, facetType);
        } else if (facetType == facetTypes[FacetType::typeOf]) {
            getOrCreateTypeOfFacet(facet, facetType);
        } else if (facetType == facetTypes[FacetType::valueRangeOf]) {
            getOrCreateValueRangeOfFacet(facet, facetType);
        } else if (facetType == facetTypes[FacetType::hasValue]) {
            getOrCreateHasValueFacet(facet, facetType);
        } else if (facetType == facetTypes[FacetType::subPropertyOf]) {
            getOrCreateSubPropertyOfFacet(facet, facetType);
        } else if (facetType == facetTypes[FacetType::domainOf]) {
            getOrCreateDomainOfFacet(facet, facetType);
        } else {
            std::cout << "FacetManager: Unknown FacetType: " << facet << std::endl;
        }
    } else {
        attachSupplementaryRule(facet);
    }
}

Facet* FacetManager::getOrCreateFacetOfFacet(const std::string& facet, const std::string& facetType)
{
    std::size_t posFirstConceptStart = facet.find_first_of('"');
    std::size_t posFirstConceptEnd = facet.find('"', posFirstConceptStart + 1);
    std::size_t posSecondConceptStart = facet.find('"', posFirstConceptEnd + 1);
    std::size_t posSecondConceptEnd = facet.find('"', posSecondConceptStart + 1);
    std::string firstConcept = facet.substr(posFirstConceptStart + 1, posFirstConceptEnd - posFirstConceptStart - 1);
    std::string secondConcept = facet.substr(posSecondConceptStart + 1, posSecondConceptEnd - posSecondConceptStart - 1);
    for (Facet* f : this->facets[FacetType::facetOf]) {
        if (f->firstConceptName == firstConcept && f->secondConceptName == secondConcept) {
            return f;
        }
    }
    this->ontologyManager->getOntologyExtractor()->createConcept(firstConcept);
    this->ontologyManager->getOntologyExtractor()->createConcept(secondConcept);
    this->ontologyManager->conceptCreated();
    Facet* ret = new Facet(firstConcept, facetType, secondConcept);
    this->facets[FacetType::facetOf].push_back(ret);
    return ret;
}

Facet* FacetManager::getOrCreateTypeOfFacet(const std::string& facet, const std::string& facetType)
{
    std::size_t posFirstConceptStart = facet.find_first_of('"');
    std::size_t posFirstConceptEnd = facet.find('"', posFirstConceptStart + 1);
    std::size_t posSecondConceptStart = facet.find('"', posFirstConceptEnd + 1);
    std::size_t posSecondConceptEnd = facet.find('"', posSecondConceptStart + 1);
    std::size_t posTypeStringStart = facet.find('"', posSecondConceptEnd + 1);
    std::size_t posTypeStringEnd = facet.find('"', posTypeStringStart + 1);
    std::string firstConcept = facet.substr(posFirstConceptStart + 1, posFirstConceptEnd - posFirstConceptStart - 1);
    std::string secondConcept = facet.substr(posSecondConceptStart + 1, posSecondConceptEnd - posSecondConceptStart - 1);
    std::string typeString = facet.substr(posTypeStringStart + 1, posTypeStringEnd - posTypeStringStart - 1);
    for (Facet* f : this->facets[FacetType::typeOf]) {
        if (f->firstConceptName == firstConcept && f->secondConceptName == secondConcept && f->typeString == typeString) {
            return f;
        }
    }
    this->ontologyManager->getOntologyExtractor()->createConcept(firstConcept);
    this->ontologyManager->getOntologyExtractor()->createConcept(secondConcept);
    this->ontologyManager->conceptCreated();
    Facet* f = new Facet(firstConcept, facetType, secondConcept);
    f->setTypeString(typeString);
    this->facets[FacetType::typeOf].push_back(f);
    return f;
}

Facet* FacetManager::getOrCreateValueRangeOfFacet(const std::string& facet, const std::string& facetType)
{
    std::size_t posFirstConceptStart = facet.find_first_of('"');
    std::size_t posFirstConceptEnd = facet.find('"', posFirstConceptStart + 1);
    std::size_t posSecondConceptStart = facet.find('"', posFirstConceptEnd + 1);
    std::size_t posSecondConceptEnd = facet.find('"', posSecondConceptStart + 1);
    std::size_t posMinStart = facet.find('"', posSecondConceptEnd + 1);
    std::size_t posMinEnd = facet.find('{', posMinStart + 1);
    std::size_t posMaxStart = facet.find('}', posMinEnd + 1);
    std::size_t posMaxEnd = facet.find('"', posMaxStart + 1);
    std::string firstConcept = facet.substr(posFirstConceptStart + 1, posFirstConceptEnd - posFirstConceptStart - 1);
    std::string secondConcept = facet.substr(posSecondConceptStart + 1, posSecondConceptEnd - posSecondConceptStart - 1);
    std::string valueRange = facet.substr(posMinEnd, posMaxStart - posMinEnd + 1);
    int minCardinality = std::stoi(facet.substr(posMinStart + 1, posMinEnd - posMinStart - 1));
    int maxCardinality = std::stoi(facet.substr(posMaxStart + 1, posMaxEnd - posMaxStart - 1));
    for (Facet* f : this->facets[FacetType::valueRangeOf]) {
        if (f->firstConceptName == firstConcept && f->secondConceptName == secondConcept && f->valueRange == valueRange &&
                f->minCardinality == minCardinality && f->maxCardinality == maxCardinality) {
            return f;
        }
    }
    this->ontologyManager->getOntologyExtractor()->createConcept(firstConcept);
    this->ontologyManager->getOntologyExtractor()->createConcept(secondConcept);
    this->ontologyManager->conceptCreated();
    Facet* f = new Facet(firstConcept, facetType, secondConcept);
    f->setValueRange(valueRange);
    f->setMinCardinality(minCardinality);
    f->setMaxCardinality(maxCardinality);
    createSupplementaryRules(f);
    this->facets[FacetType::valueRangeOf].push_back(f);
    return f;
}

Facet* FacetManager::getOrCreateHasValueFacet(const std::string& facet, const std::string& facetType)
{
    std::size_t posIndividualStart = facet.find_first_of('"');
    std::size_t posIndividualEnd = facet.find('"', posIndividualStart + 1);
    std::size_t posFirstConceptStart = facet.find_first_of('"', posIndividualEnd + 1);
    std::size_t posFirstConceptEnd = facet.find('"', posFirstConceptStart + 1);
    std::size_t posSecondConceptStart = facet.find('"', posFirstConceptEnd + 1);
    std::size_t posSecondConceptEnd = facet.find('"', posSecondConceptStart + 1);
    std::size_t posValueStart = facet.find('"', posSecondConceptEnd + 1);
    std::size_t posValueEnd = facet.find('"', posValueStart + 1);
    std::size_t posTimeStampStart = facet.find(',', posValueEnd + 1);
    std::size_t posTimeStampEnd = facet.find(')', posTimeStampStart + 1);
    std::string firstConcept = facet.substr(posFirstConceptStart + 1, posFirstConceptEnd - posFirstConceptStart - 1);
    std::string secondConcept = facet.substr(posSecondConceptStart + 1, posSecondConceptEnd - posSecondConceptStart - 1);
    std::string individualName = facet.substr(posIndividualStart + 1, posIndividualEnd - posIndividualStart - 1);
    std::string value = facet.substr(posValueStart + 1, posValueEnd - posValueStart - 1);
    int timeStep = std::stoi(Facet::trim(facet.substr(posTimeStampStart + 1, posTimeStampEnd - posTimeStampStart - 1)));
    for (Facet* f : this->facets[FacetType::hasValue]) {
        if (f->firstConceptName == firstConcept && f->secondConceptName == secondConcept && f->individualName == individualName &&
                f->value == value && f->timeStep == timeStep) {
            return f;
        }
    }
    this->ontologyManager->getOntologyExtractor()->createConcept(firstConcept);
    this->ontologyManager->getOntologyExtractor()->createConcept(secondConcept);
    this->ontologyManager->conceptCreated();
    Facet* f = new Facet(firstConcept, facetType, secondConcept);
    f->setIndividualName(individualName);
    f->setValue(value);
    f->setTimeStep(timeStep);
    this->facets[FacetType::hasValue].push_back(f);
    return f;
}

Facet* FacetManager::getOrCreateSubPropertyOfFacet(const std::string& facet, const std::string& facetType)
{
    std::size_t posFirstConceptStart = facet.find_first_of('"');
    std::size_t posFirstConceptEnd = facet.find('"', posFirstConceptStart + 1);
    std::size_t posSecondConceptStart = facet.find('"', posFirstConceptEnd + 1);
    std::size_t posSecondConceptEnd = facet.find('"', posSecondConceptStart + 1);
    std::size_t posTimeStampStart = facet.find(',', posSecondConceptEnd + 1);
    std::size_t posTimeStampEnd = facet.find(')', posTimeStampStart + 1);
    std::string firstConcept = facet.substr(posFirstConceptStart + 1, posFirstConceptEnd - posFirstConceptStart - 1);
    std::string secondConcept = facet.substr(posSecondConceptStart + 1, posSecondConceptEnd - posSecondConceptStart - 1);
    int timeStep = std::stoi(Facet::trim(facet.substr(posTimeStampStart + 1, posTimeStampEnd - posTimeStampStart - 1)));
    for (Facet* f : this->facets[FacetType::subPropertyOf]) {
        if (f->firstConceptName == firstConcept && f->secondConceptName == secondConcept && f->timeStep == timeStep) {
            return f;
        }
    }
    this->ontologyManager->getOntologyExtractor()->createConcept(firstConcept);
    this->ontologyManager->getOntologyExtractor()->createConcept(secondConcept);
    this->ontologyManager->conceptCreated();
    Facet* f = new Facet(firstConcept, facetType, secondConcept);
    f->setTimeStep(timeStep);
    this->facets[FacetType::subPropertyOf].push_back(f);
    return f;
}

Facet* FacetManager::getOrCreateDomainOfFacet(const std::string& facet, const std::string& facetType)
{
    std::size_t posFirstConceptStart = facet.find_first_of('"');
    std::size_t posFirstConceptEnd = facet.find('"', posFirstConceptStart + 1);
    std::size_t posSecondConceptStart = facet.find('"', posFirstConceptEnd + 1);
    std::size_t posSecondConceptEnd = facet.find('"', posSecondConceptStart + 1);
    std::size_t posTimeStampStart = facet.find(',', posSecondConceptEnd + 1);
    std::size_t posTimeStampEnd = facet.find(')', posTimeStampStart + 1);
    std::string firstConcept = facet.substr(posFirstConceptStart + 1, posFirstConceptEnd - posFirstConceptStart - 1);
    std::string secondConcept = facet.substr(posSecondConceptStart + 1, posSecondConceptEnd - posSecondConceptStart - 1);
    int timeStep = std::stoi(Facet::trim(facet.substr(posTimeStampStart + 1, posTimeStampEnd - posTimeStampStart - 1)));
    for (Facet* f : this->facets[FacetType::domainOf]) {
        if (f->firstConceptName == firstConcept && f->secondConceptName == secondConcept && f->timeStep == timeStep) {
            return f;
        }
    }
    this->ontologyManager->getOntologyExtractor()->createConcept(firstConcept);
    this->ontologyManager->getOntologyExtractor()->createConcept(secondConcept);
    Facet* f = new Facet(firstConcept, facetType, secondConcept);
    f->setTimeStep(timeStep);
    this->facets[FacetType::domainOf].push_back(f);
    return f;
}

void FacetManager::attachSupplementaryRule(const std::string& facet)
{
    std::size_t posFirstConceptStart = facet.find_first_of('"');
    std::size_t posFirstConceptEnd = facet.find('"', posFirstConceptStart + 1);
    std::size_t posSecondConceptStart = facet.find('"', posFirstConceptEnd + 1);
    std::size_t posSecondConceptEnd = facet.find('"', posSecondConceptStart + 1);
    std::string firstConcept = facet.substr(posFirstConceptStart + 1, posFirstConceptEnd - posFirstConceptStart - 1);
    std::string secondConcept = facet.substr(posSecondConceptStart + 1, posSecondConceptEnd - posSecondConceptStart - 1);
    for (Facet* f : this->facets[FacetType::valueRangeOf]) {
        if (f->firstConceptName == firstConcept && f->secondConceptName == secondConcept) {
            f->setSupplementaryRule(facet);
            break;
        }
    }
}

std::pair<std::string, std::string> FacetManager::translateFacets()
{
    std::pair<std::string, std::string> ret;
    std::stringstream stream;
    for (const auto& facet : getFacets(false)) {
        stream << facet;
    }
    ret.first = stream.str();
    stream.str("");
    for (auto facet : this->facets[FacetType::valueRangeOf]) {
        for (std::string rule : facet->supplementaryRules) {
            stream << rule << "\n";
        }
    }
    ret.second = stream.str();
    return ret;
}

std::vector<std::string> FacetManager::getFacets(bool translateSupplementary)
{

    std::vector<std::string> ret;
    std::stringstream stream;
    for (auto facet : this->facets[FacetType::facetOf]) {
        stream << "#external " << facetTypes[FacetType::facetOf] << "(\"" << facet->firstConceptName << "\",\"" << facet->secondConceptName
               << "\").\n";
        ret.push_back(stream.str());
        stream.str("");
    }
    for (auto facet : this->facets[FacetType::typeOf]) {
        stream << "#external " << facetTypes[FacetType::typeOf] << "(\"" << facet->firstConceptName << "\",\"" << facet->secondConceptName
               << "\",\"" << facet->typeString << "\").\n";
        ret.push_back(stream.str());
        stream.str("");
    }
    for (auto facet : this->facets[FacetType::valueRangeOf]) {
        stream << "#external " << facetTypes[FacetType::valueRangeOf] << "(\"" << facet->firstConceptName << "\",\""
               << facet->secondConceptName << "\",\"" << facet->minCardinality << facet->valueRange << facet->maxCardinality << "\").\n";
        if (translateSupplementary) {
            for (std::string rule : facet->supplementaryRules) {
                stream << rule << "\n";
            }
        }
        ret.push_back(stream.str());
        stream.str("");
    }
    for (auto facet : this->facets[FacetType::hasValue]) {
        stream << "#external " << facetTypes[FacetType::hasValue] << "(\"" << facet->individualName << "\",\"" << facet->firstConceptName
               << "\",\"" << facet->secondConceptName << "\",\"" << facet->value << "\"," << facet->timeStep << ").\n";
        ret.push_back(stream.str());
        stream.str("");
    }

    for (auto facet : this->facets[FacetType::subPropertyOf]) {
        stream << "#external " << facetTypes[FacetType::subPropertyOf] << "(\"" << facet->firstConceptName << "\",\""
               << facet->secondConceptName << "\"," << facet->timeStep << ").\n";
        ret.push_back(stream.str());
        stream.str("");
    }
    for (auto facet : this->facets[FacetType::domainOf]) {
        stream << "#external " << facetTypes[FacetType::domainOf] << "(\"" << facet->firstConceptName << "\",\"" << facet->secondConceptName
               << "\"," << facet->timeStep << ").\n";
        ret.push_back(stream.str());
        stream.str("");
    }
    return ret;
}

void FacetManager::deleteFacet(const std::string& facetString)
{
    std::size_t posFacetTypeStart = facetString.find_first_of(' ');
    std::size_t posFacetTypeEnd = facetString.find('(', posFacetTypeStart + 1);
    std::string facetType = facetString.substr(posFacetTypeStart + 1, posFacetTypeEnd - posFacetTypeStart - 1);
    FacetType type = Facet::getFacetType(facetType);
    std::vector<Facet*> vector;
    Facet* facet;
    switch (type) {
    case FacetType::facetOf:
        facet = getOrCreateFacetOfFacet(facetString, facetType);
        vector = this->facets[FacetType::facetOf];
        vector.erase(std::find(vector.begin(), vector.end(), facet));
        this->facets[FacetType::facetOf] = vector;
        delete facet;
        break;
    case FacetType::typeOf:
        facet = getOrCreateTypeOfFacet(facetString, facetType);
        vector = this->facets[FacetType::typeOf];
        vector.erase(std::find(vector.begin(), vector.end(), facet));
        this->facets[FacetType::typeOf] = vector;
        delete facet;
        break;
    case FacetType::valueRangeOf:
        facet = getOrCreateValueRangeOfFacet(facetString, facetType);
        vector = this->facets[FacetType::valueRangeOf];
        vector.erase(std::find(vector.begin(), vector.end(), facet));
        this->facets[FacetType::valueRangeOf] = vector;
        delete facet;
        break;
    case FacetType::hasValue:
        facet = getOrCreateHasValueFacet(facetString, facetType);
        vector = this->facets[FacetType::hasValue];
        vector.erase(std::find(vector.begin(), vector.end(), facet));
        this->facets[FacetType::hasValue] = vector;
        delete facet;
        break;
    case FacetType::subPropertyOf:
        facet = getOrCreateSubPropertyOfFacet(facetString, facetType);
        vector = this->facets[FacetType::subPropertyOf];
        vector.erase(std::find(vector.begin(), vector.end(), facet));
        this->facets[FacetType::subPropertyOf] = vector;
        delete facet;
        break;
    case FacetType::domainOf:
        facet = getOrCreateDomainOfFacet(facetString, facetType);
        vector = this->facets[FacetType::domainOf];
        vector.erase(std::find(vector.begin(), vector.end(), facet));
        this->facets[FacetType::domainOf] = vector;
        delete facet;
        break;
    default:
        std::cout << "FacetManager: deleteFacet: Type " << facetType << " not supported!" << std::endl;
    }
}

void FacetManager::createFacet(const std::string& fromConcept, const std::string& relation, const std::string& toConcept,
        const std::string& typeString, const std::string& valueRange, int minCard, int maxCard, const std::string& individual,
        const std::string& value, long timeStep)
{
    FacetType type = Facet::getFacetType(relation);
    Facet* facet;
    switch (type) {
    case FacetType::facetOf:
        for (Facet* f : this->facets[FacetType::facetOf]) {
            if (f->firstConceptName == fromConcept && f->secondConceptName == toConcept) {
                return;
            }
        }
        this->ontologyManager->getOntologyExtractor()->createConcept(fromConcept);
        this->ontologyManager->getOntologyExtractor()->createConcept(toConcept);
        facet = new Facet(fromConcept, relation, toConcept);
        this->facets[FacetType::facetOf].push_back(facet);
        break;
    case FacetType::typeOf:
        for (Facet* f : this->facets[FacetType::typeOf]) {
            if (f->firstConceptName == fromConcept && f->secondConceptName == toConcept && f->typeString == typeString) {
                return;
            }
        }
        this->ontologyManager->getOntologyExtractor()->createConcept(fromConcept);
        this->ontologyManager->getOntologyExtractor()->createConcept(toConcept);
        facet = new Facet(fromConcept, relation, toConcept);
        facet->setTypeString(typeString);
        this->facets[FacetType::typeOf].push_back(facet);
        break;
    case FacetType::valueRangeOf:
        for (Facet* f : this->facets[FacetType::valueRangeOf]) {
            if (f->firstConceptName == fromConcept && f->secondConceptName == toConcept && f->valueRange == valueRange &&
                    f->minCardinality == minCard && f->maxCardinality == maxCard) {
                return;
            }
        }
        this->ontologyManager->getOntologyExtractor()->createConcept(fromConcept);
        this->ontologyManager->getOntologyExtractor()->createConcept(toConcept);
        facet = new Facet(fromConcept, relation, toConcept);
        facet->setValueRange(valueRange);
        facet->setMinCardinality(minCard);
        facet->setMaxCardinality(maxCard);
        createSupplementaryRules(facet);
        this->facets[FacetType::valueRangeOf].push_back(facet);
        break;
    case FacetType::hasValue:
        for (Facet* f : this->facets[FacetType::hasValue]) {
            if (f->firstConceptName == fromConcept && f->secondConceptName == toConcept && f->individualName == individual &&
                    f->value == value && f->timeStep == timeStep) {
                return;
            }
        }
        this->ontologyManager->getOntologyExtractor()->createConcept(fromConcept);
        this->ontologyManager->getOntologyExtractor()->createConcept(toConcept);
        facet = new Facet(fromConcept, relation, toConcept);
        facet->setIndividualName(individual);
        facet->setValue(value);
        facet->setTimeStep(timeStep);
        this->facets[FacetType::hasValue].push_back(facet);
        break;
    case FacetType::subPropertyOf:
        for (Facet* f : this->facets[FacetType::subPropertyOf]) {
            if (f->firstConceptName == fromConcept && f->secondConceptName == toConcept && f->timeStep == timeStep) {
                return;
            }
        }
        this->ontologyManager->getOntologyExtractor()->createConcept(fromConcept);
        this->ontologyManager->getOntologyExtractor()->createConcept(toConcept);
        facet = new Facet(fromConcept, relation, toConcept);
        facet->setTimeStep(timeStep);
        this->facets[FacetType::subPropertyOf].push_back(facet);
        break;
    case FacetType::domainOf:
        for (Facet* f : this->facets[FacetType::domainOf]) {
            if (f->firstConceptName == fromConcept && f->secondConceptName == toConcept && f->timeStep == timeStep) {
                return;
            }
        }
        this->ontologyManager->getOntologyExtractor()->createConcept(fromConcept);
        this->ontologyManager->getOntologyExtractor()->createConcept(toConcept);
        facet = new Facet(fromConcept, relation, toConcept);
        facet->setTimeStep(timeStep);
        this->facets[FacetType::domainOf].push_back(facet);
        break;
    default:
        std::cout << "FacetManager: deleteFacet: Type " << relation << " not supported!" << std::endl;
    }
}

void FacetManager::createSupplementaryRules(Facet* facet)
{
    // maxCardinality
    // propertyViolation(Individual,"threatLevel","dangerous","Too many Values") :- X = #count{Value :
    // hasValue(Individual,"threatLevel","dangerous",Value,_)}, hasValue(Individual,"threatLevel","dangerous",_,_), X > 1.
    std::stringstream stream;
    stream << facetTypes[FacetType::propertyViolation] << "(Individual,\"" << facet->firstConceptName << "\",\"" << facet->secondConceptName
           << "\",\"Too many Values\")";
    stream << " :- X = #count{Value : " << facetTypes[FacetType::hasValue] << "(Individual, \"" << facet->firstConceptName << "\",\""
           << facet->secondConceptName << "\",Value,_)}, ";
    stream << facetTypes[FacetType::hasValue] << "(Individual, \"" << facet->firstConceptName << "\",\"" << facet->secondConceptName
           << "\",_,_), ";
    stream << " X > " << facet->maxCardinality << "," << facetTypes[FacetType::valueRangeOf] << "(\"" << facet->firstConceptName << "\",\""
           << facet->secondConceptName << "\",\"" << facet->getMinCardinality() << facet->getValueRange() << facet->getMaxCardinality()
           << "\").";
    facet->supplementaryRules.emplace_back(stream.str());
    stream.str("");

    if (facet->minCardinality > 0) {
        // propertyViolation(Individual,"threatLevel","dangerous","Too few Values") :- X = #count{Value :
        // hasValue(Individual,"threatLevel","dangerous",Value,_)}, hasValue(Individual,"threatLevel","dangerous",_,_), X < 1.
        stream << facetTypes[FacetType::propertyViolation] << "(Individual,\"" << facet->firstConceptName << "\",\""
               << facet->secondConceptName << "\",\"Too few Values\")";
        stream << " :- X = #count{Value : " << facetTypes[FacetType::hasValue] << "(Individual, \"" << facet->firstConceptName << "\",\""
               << facet->secondConceptName << "\",Value,_)}, ";
        stream << facetTypes[FacetType::hasValue] << "(Individual, \"" << facet->firstConceptName << "\",\"" << facet->secondConceptName
               << "\",_,_), ";
        stream << " X < " << facet->minCardinality << "," << facetTypes[FacetType::valueRangeOf] << "(\"" << facet->firstConceptName
               << "\",\"" << facet->secondConceptName << "\",\"" << facet->getMinCardinality() << facet->getValueRange()
               << facet->getMaxCardinality() << "\").";
        facet->supplementaryRules.emplace_back(stream.str());
        stream.str("");
        // propertyViolation(Individual, "threatLevel","dangerous","Too few Values") :- not
        // hasValue(Individual,"threatLevel","dangerous",_,_), is(Individual,_).
        stream << facetTypes[FacetType::propertyViolation] << "(Individual,\"" << facet->firstConceptName << "\",\""
               << facet->secondConceptName << "\",\"Too few Values\")";
        stream << " :- not " << facetTypes[FacetType::hasValue] << "(Individual, \"" << facet->firstConceptName << "\",\""
               << facet->secondConceptName << "\",_,_), ";
        stream << "is(Individual,\"" << facet->secondConceptName << "\")"
               << "," << facetTypes[FacetType::valueRangeOf] << "(\"" << facet->firstConceptName << "\",\"" << facet->secondConceptName
               << "\",\"" << facet->getMinCardinality() << facet->getValueRange() << facet->getMaxCardinality() << "\").";
        ;
        facet->supplementaryRules.emplace_back(stream.str());
    }
}

int FacetManager::getNumberOfFacets() {
    int ret = 0;
    for (auto pair : this->facets) {
        ret += pair.second.size();
    }
    return ret;
}
