#include "model/Individual.h"

#include <utility>

Individual::Individual(
        std::string name, std::string baseConceptName, conceptnet::Concept* baseConcept, std::string baseRelation, int weight)
        : name(std::move(name))
        , baseConceptName(std::move(baseConceptName))
        , baseRelation(std::move(baseRelation))
        , baseConcept(baseConcept)
        , weight(weight)
{
}

const std::string& Individual::getName() const
{
    return this->name;
}

const std::string& Individual::getBaseConceptName() const
{
    return this->baseConceptName;
}

conceptnet::Concept* Individual::getBaseConcept() const
{
    return this->baseConcept;
}

const std::string& Individual::getBaseRelation() const
{
    return this->baseRelation;
}

int Individual::getWeight() const
{
    return this->weight;
}
