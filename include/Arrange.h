#pragma once

#include <wx/wxprec.h>
#include <wx/wx.h>
#include <wx/textctrl.h>

#include <iostream>

class OntologyManager;
class MainController;
class SolveController;
class MainFrame;
class Arrange : public wxApp {
public:
    bool OnInit() override;
    void CleanUp() override;

private:
    MainController* controller;
    SolveController* solveController;
    OntologyManager* ontologyManager;
    MainFrame* frame;
};