#pragma once

#include "view/tabs/SolutionTab.h"

#include <wx/bookctrl.h>
#include <wx/listctrl.h>
#include <wx/textctrl.h>
#include <wx/wx.h>
#include <wx/wxprec.h>

#include <iostream>

class Arrange;
class IArrangeController;
class ISolverController;
class IndividualsTab;
class OntologyTab;
class OntologyRulesTab;
class OntologyProgramTab;
class SolutionTab;
class FacetsTab;
class MainFrame : public wxFrame
{
    friend class Arrange;

public:
    explicit MainFrame(const wxString& title);
    void init(IArrangeController* controller, ISolverController* solverController);
    void ontologyCreationFinished();
    [[nodiscard]] wxListBox* getConceptListbox() const;

    [[nodiscard]] wxTextCtrl* getOntologyTextControl() const;
    [[nodiscard]] wxTextCtrl* getOntologyRulesTextControl() const;
    [[nodiscard]] IndividualsTab* getIndividualsTab() const;
    [[nodiscard]] SolutionTab* getSolutionTab() const;
    [[nodiscard]] OntologyTab* getOntologyTab() const;
    [[nodiscard]] const std::string& getResourcesPath() const;
    [[nodiscard]] FacetsTab* getFacetsTab() const;

private:
    void initialiseMenuBar();
    void initialiseBook();
    void initialiseStatusBar();
    void OnQuit(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);
    void OnLoadOntology(wxCommandEvent& event);
    void OnLoadFacets(wxCommandEvent& event);
    void OnLoadIndividuals(wxCommandEvent& event);
    void OnCreateOntology(wxCommandEvent& event);
    void OnTimerEvent(wxTimerEvent& event);
    void OnSizeEvent(wxSizeEvent& event);
    void OnSolve(wxCommandEvent& event);
    void OnNewOntology(wxCommandEvent& event);
    void OnSettings(wxCommandEvent& event);

    IArrangeController* arrangeController;
    ISolverController* solverController;

    wxMenu* fileMenu;
    wxMenu* solveMenu;
    wxMenu* settingsMenu;
    wxMenu* helpMenu;
    wxMenuBar* menuBar;
    wxBookCtrl* book;
    wxGauge* progressGauge;
    wxStatusBar* statusBar;
    wxTimer* progressTimer;
    IndividualsTab* individualsTab;
    OntologyProgramTab* ontologyProgramTab;
    OntologyRulesTab* ontologyRulesTab;
    OntologyTab* ontologyTab;
    SolutionTab* solutionTab;
    FacetsTab* facetsTab;

    std::string resourcesPath;

    DECLARE_EVENT_TABLE();

    void startProgressGauge() const;

    void stopProgressGauge() const;

    void clearLists() const;
};
