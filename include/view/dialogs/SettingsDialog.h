#pragma once

#include <wx/combobox.h>
#include <wx/dialog.h>
#include <wx/spinctrl.h>
#include <wx/textctrl.h>

class IArrangeController;
class SettingsDialog : public wxDialog
{

public:
    SettingsDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
    void init(IArrangeController* controller);

private:
    void OnCancel(wxCommandEvent& event);
    void OnApply(wxCommandEvent&);
    IArrangeController* createOntologyController;
    wxButton* applyButton;
    wxButton* cancelButton;
    wxSpinCtrlDouble* isAWeightSpinCtrl;
    wxSpinCtrlDouble* formOfWeightSpinCtrl;
    wxSpinCtrlDouble* synonymWeightSpinCtrl;
    wxSpinCtrlDouble* hasPropertyWeightSpinCtrl;
    wxSpinCtrlDouble* minRelatednessSpinCtrl;
};