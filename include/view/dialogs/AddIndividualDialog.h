#pragma once

#include <wx/button.h>
#include <wx/choice.h>
#include <wx/dialog.h>
#include <wx/textctrl.h>

class IArrangeController;
class AddIndividualDialog : public wxDialog
{

public:
    AddIndividualDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
    void init(IArrangeController* controller);

private:
    void OnCancel(wxCommandEvent& event);
    void OnCreate(wxCommandEvent& event);
    IArrangeController* createOntologyController;
    wxButton* createButton;
    wxButton* cancelButton;
    wxTextCtrl* individualName;
    wxChoice* baseConcepts;
};
