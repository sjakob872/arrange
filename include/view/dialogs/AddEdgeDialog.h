#pragma once

#include <wx/choice.h>
#include <wx/combobox.h>
#include <wx/dialog.h>
#include <wx/spinctrl.h>
#include <wx/textctrl.h>

class IArrangeController;
class AddEdgeDialog : public wxDialog
{

public:
    AddEdgeDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
    void init(IArrangeController* controller);

private:
    void OnCancel(wxCommandEvent& event);
    void OnCreate(wxCommandEvent& event);
    IArrangeController* createOntologyController;
    wxButton* createButton;
    wxButton* cancelButton;
    wxComboBox* fromConceptNameCmbBox;
    wxComboBox* toConceptNameCmbBox;
    wxChoice* relationChoice;
    wxSpinCtrlDouble* weightSpinCtrl;
};
