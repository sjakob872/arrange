#pragma once

#include <wx/button.h>
#include <wx/choice.h>
#include <wx/dialog.h>
#include <wx/textctrl.h>

class IArrangeController;
class EditIndividualDialog : public wxDialog
{

public:
    EditIndividualDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);

    void init(IArrangeController* controller, std::string selectedIndividual, std::string selectedIndividualConcept);

private:
    void OnCancel(wxCommandEvent& event);
    void OnEdit(wxCommandEvent& event);

    IArrangeController* createOntologyController;
    std::string selectedIndividualOldName;
    std::string selectedIndividualConcept;
    wxButton* editButton;
    wxButton* cancelButton;
    wxTextCtrl* individualName;
    wxChoice* baseConcepts;
};