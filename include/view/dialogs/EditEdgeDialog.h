#pragma once

#include <wx/choice.h>
#include <wx/combobox.h>
#include <wx/dialog.h>
#include <wx/spinctrl.h>
#include <wx/textctrl.h>

class IArrangeController;
class EditEdgeDialog : public wxDialog
{
public:
    EditEdgeDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
    void init(IArrangeController* controller, const std::string& startConcept, const std::string& relation, const std::string& endConcept, double weight,
            const std::string& id);

private:
    void OnCancel(wxCommandEvent& event);
    void OnEdit(wxCommandEvent&);
    IArrangeController* createOntologyController{};
    wxButton* editButton;
    wxButton* cancelButton;
    wxComboBox* fromConceptNameCmbBox;
    wxComboBox* toConceptNameCmbBox;
    wxChoice* relationChoice;
    wxSpinCtrlDouble* weightSpinCtrl;
    std::string selectedStartConcept;
    std::string selectedRelation;
    std::string selectedEndConcept;
    double selectedWeight;
    std::string selectedId;
};
