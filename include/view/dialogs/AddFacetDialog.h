#pragma once

#include <wx/combobox.h>
#include <wx/dialog.h>
#include <wx/spinctrl.h>
#include <wx/textctrl.h>

class IArrangeController;
class AddFacetDialog : public wxDialog
{

public:
    AddFacetDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
    void init(IArrangeController* controller);

private:
    void OnCancel(wxCommandEvent& event);
    void OnCreate(wxCommandEvent& event);
    void enable(wxCommandEvent& event);
    IArrangeController* createOntologyController;
    wxButton* createButton;
    wxButton* cancelButton;
    wxComboBox* fromConceptNameCmbBox;
    wxChoice* relationChoice;
    wxChoice* toConceptNameChoice;

    wxChoice* individualChoice;
    wxSpinCtrl* minCardSpinCtrl;
    wxSpinCtrl* maxCardSpinCtrl;
    wxSpinCtrl* timeStepSpinCtrl;
    wxTextCtrl* valueRangeTxtCtrl;
    wxTextCtrl* valueTxtCtrl;
    wxTextCtrl* typeStringTxtCtrl;
};