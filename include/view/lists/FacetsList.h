#pragma once

#include <wx/listctrl.h>

#include <vector>

class FacetsList : public wxListCtrl
{
public:
    explicit FacetsList(wxWindow* parent, wxWindowID id = wxID_ANY);
    void addFacet(const std::string& facet);
    void addFacets(const std::vector<std::string>& facets);
    void deleteFacet(const std::string& facet);
    void clearFacets();

protected:
    [[nodiscard]] wxString OnGetItemText(long item, long column) const override;
    void OnSize(wxSizeEvent& event);

private:
    std::vector<std::string> facets;
};
