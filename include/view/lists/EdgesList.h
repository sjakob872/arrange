#pragma once

#include <wx/listctrl.h>

#include <tuple>
#include <vector>

class EdgesList : public wxListCtrl
{
public:
    explicit EdgesList(wxWindow* parent, wxWindowID id = wxID_ANY);

    void addEdge(const std::string& startConcept, const std::string& relation, const std::string& endConcept, double weight, long id);
    void addEdge(const std::tuple<std::string, std::string, std::string, double, long>& edge);
    void editEdge(const std::string& oldStartConcept, const std::string& newStartConcept, const std::string& oldRelation,
            const std::string& newRelation, const std::string& oldEndConcept, const std::string& newEndConcept, double weight, long id);
    void deleteEdge(const std::string& startConcept, const std::string& relation, const std::string& endConcept, long id);
    void clearEdges();

    [[nodiscard]] const std::string& getSelectedConcept() const;
    void setSelectedConcept(const std::string& selectedConcept);

protected:
    [[nodiscard]] wxString OnGetItemText(long item, long column) const override;

private:
    std::vector<std::tuple<std::string, std::string, std::string, double, long>> edges;
    std::string selectedConcept;
};
