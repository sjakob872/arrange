#pragma once

#include <wx/listctrl.h>

#include <vector>

class IndividualsList : public wxListCtrl
{
public:
    explicit IndividualsList(wxWindow* parent, wxWindowID id = wxID_ANY);
    void addIndividual(const std::string& individualName, const std::string& baseConcept);
    void editIndividual(const std::string& oldIndividualName, const std::string& newIndividualName, const std::string& baseConcept);
    void deleteIndividual(const std::string& individual);

    void clearIndividuals();

protected:
    [[nodiscard]] wxString OnGetItemText(long item, long column) const override;

private:
    std::vector<std::pair<std::string, std::string>> individuals;
};
