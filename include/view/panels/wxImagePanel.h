#pragma once

#include <wx/sizer.h>
#include <wx/wx.h>

class wxImagePanel : public wxScrolledWindow
{
public:
    wxImagePanel(wxWindow* parent, const wxString& file, wxBitmapType format);

    void paintEvent(wxPaintEvent& evt);
    void paintNow();
    void OnSize(wxSizeEvent& event);
    void render(wxDC& dc);

private:
    void OnLeftDown(wxMouseEvent& evt);
    void OnRightDown(wxMouseEvent& evt);
    void AddZoom(double zoomFactor, const wxPoint& center);
    void SetZoom(double zoomFactor, const wxPoint& center);
    void OnScroll(wxScrollWinEvent& evt);
    void updateVirtualSize();
    wxImage image;
    bool initialised;
    double zoom;
    double offsetX;
    double offsetY;

    // some useful events
    /*
     void mouseMoved(wxMouseEvent& event);
     void mouseDown(wxMouseEvent& event);
     void mouseReleased(wxMouseEvent& event);
     void rightClick(wxMouseEvent& event);
     void mouseLeftWindow(wxMouseEvent& event);
     void keyPressed(wxKeyEvent& event);
     void keyReleased(wxKeyEvent& event);
     */

    DECLARE_EVENT_TABLE()
};
