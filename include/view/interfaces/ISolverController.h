#pragma once

#include <vector>

class ISolverController
{
public:
    virtual void solve() = 0;
    virtual std::vector<int> getStatistics() = 0;
};
