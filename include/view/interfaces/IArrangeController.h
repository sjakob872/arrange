#pragma once

#include <string>
#include <tuple>
#include <vector>

class IArrangeController
{

public:
    virtual void createCN5Ontology(std::string& rootConcept) = 0;
    virtual void createNewOntology() = 0;
    virtual void loadOntology(std::string ontology) = 0;

    virtual void createIndividual(std::string individual, bool load) = 0;
    virtual void createIndividual(std::string individualName, std::string baseConcept) = 0;
    virtual void editIndividual(std::string individualOldName, std::string individualNewName, std::string baseConcept) = 0;
    virtual void deleteIndividual(std::string individual) = 0;

    virtual void createEdge(std::string fromConcept, std::string relation, std::string toConcept, double weight) = 0;
    virtual void editEdge(std::string oldStartConcept, std::string newStartConcept, std::string oldRelation, std::string newRelation,
            std::string oldEndConcept, std::string newEndConcept, double weight, long id) = 0;
    virtual void deleteEdge(std::string startConcept, std::string relation, std::string endConcept, long id) = 0;

    virtual void createFacets(std::vector<std::string> facets, bool load) = 0;
    virtual void createFacet(std::string fromConcept, std::string relation, std::string toConcept, std::string typeString,
            std::string valueRange, int minCard, int maxCard, std::string individual, std::string value, long timeStep) = 0;
    virtual void deleteFacet(std::string facet) = 0;

    virtual void applySettings(
            double isAWeight, double formOfWeight, double synonymWeight, double hasPropertyWeight, double minRelatedness) = 0;

    virtual std::vector<std::string> getAvailableConcepts() = 0;
    virtual std::vector<std::string> getAvailableIndividuals() = 0;
    virtual std::vector<std::string> getOntologyRelations() = 0;
    virtual std::vector<std::string> getFacetRelations(bool skip) = 0;
    virtual std::string getIndividualsASPProgram() = 0;
    virtual std::pair<std::string,std::string> getFacetsASPProgram() = 0;
    virtual std::string getOntologyProgram() = 0;
    virtual std::vector<std::tuple<std::string, std::string, std::string, double, long>> getConnectedEdges(std::string conceptName) = 0;
};
