#pragma once

class GUIIdentifiers
{
public:
    static const int BOOK_CTRL;
    static const int CONCEPT_LIST;
    static const int ONTOLOGY_TEXTBOX;
    static const int LOAD_ONTOLOGY_FILE;
    static const int CREATE_ONTOLOGY;
    static const int PROGRESS_BAR_TIMER;
    static const int RULE_TEXTBOX;
    static const int LOAD_FACET_FILE;
    static const int LOAD_INDIVIDUALS_FILE;
    static const int SOLVE;
    static const int INDIVIDUALS_LIST;
    static const int ADD_INDIVIDUAL_BUTTON;
    static const int EDIT_INDIVIDUAL_BUTTON;
    static const int DELETE_INDIVIDUAL_BUTTON;
    static const int SAVE_INDIVIDUALS_BUTTON;
    static const int NEW_ONTOLOGY;
    static const int ADD_EDGE_BUTTON;
    static const int EDIT_EDGE_BUTTON;
    static const int DELETE_EDGE_BUTTON;
    static const int SAVE_EDGES_BUTTON;
    static const int SAVE_RULES_BUTTON;
    static const int EDGES_LIST;
    static const int ADD_FACET_BUTTON;
    static const int DELETE_FACET_BUTTON;
    static const int SAVE_FACETS_BUTTON;
    static const int FACETS_LIST;
    static const int SETTINGS;
    static const int HELP_ABOUT;
    static const int FILE_QUIT;
};
