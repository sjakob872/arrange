#pragma once

#include "view/tabs/Tab.h"

#include <wx/button.h>
#include <wx/listctrl.h>

class FacetsList;
class IArrangeController;
class FacetsTab : public Tab
{
public:
    explicit FacetsTab(wxWindow* parent, wxWindowID winid = wxID_ANY, const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL | wxNO_BORDER, const wxString& name = wxPanelNameStr);

    void init(IArrangeController* controller);
    void enable();

    void OnAddFacetsButton(wxCommandEvent& event);
    void OnDeleteFacetsButton(wxCommandEvent& event);
    void OnSaveFacetsButton(wxCommandEvent& event);

    [[nodiscard]] FacetsList* getFacetsListControl() const;

private:
    IArrangeController* controller;

    FacetsList* facetsListControl;

    wxButton* addFacetsButton;
    wxButton* deleteFacetsButton;
    wxButton* saveFacetsButton;
};
