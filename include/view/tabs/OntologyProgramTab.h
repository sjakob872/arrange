#pragma once

#include "view/tabs/Tab.h"

#include <wx/button.h>
#include <wx/textctrl.h>

class OntologyProgramTab : public Tab
{
public:
    explicit OntologyProgramTab(wxWindow* parent, wxWindowID winid = wxID_ANY, const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL | wxNO_BORDER, const wxString& name = wxPanelNameStr);

    void init();
    void enable();

    void OnSaveRulesButton(wxCommandEvent& event);

    [[nodiscard]] wxTextCtrl* getOntologyTextControl() const;

private:
    wxTextCtrl* ontologyTextControl;
    wxButton* saveProgramButton;
};
