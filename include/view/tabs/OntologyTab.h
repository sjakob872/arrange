#pragma once

#include "view/tabs/Tab.h"

#include <wx/button.h>
#include <wx/listbox.h>
#include <wx/listctrl.h>
#include <wx/textctrl.h>

class EdgesList;
class IArrangeController;
class OntologyTab : public Tab
{
public:
    explicit OntologyTab(wxWindow* parent, wxWindowID winid = wxID_ANY, const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL | wxNO_BORDER, const wxString& name = wxPanelNameStr);

    void init(IArrangeController* controller);
    void enable();

    void OnAddEdgeButton(wxCommandEvent& event);
    void OnEditEdgeButton(wxCommandEvent& event);
    void OnDeleteEdgeButton(wxCommandEvent& event);
    void OnSaveEdgesButton(wxCommandEvent& event);
    void OnListBoxDoubleClick(wxCommandEvent& event);

    [[nodiscard]] wxListBox* getConceptListbox() const;
    [[nodiscard]] EdgesList* getEdgesListControl() const;

private:
    IArrangeController* arrangeController;
    wxListBox* conceptListbox;
    EdgesList* edgesListControl;
    wxButton* addEdgeButton;
    wxButton* editEdgeButton;
    wxButton* deleteEdgeButton;
    wxButton* saveEdgesButton;
};
