#pragma once

#include "view/tabs/Tab.h"

#include <wx/textctrl.h>
#include <wx/wx.h>

class wxImagePanel;
class SolutionTab : public Tab
{
public:
    explicit SolutionTab(wxWindow* parent, wxWindowID winid = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
            long style = wxTAB_TRAVERSAL | wxNO_BORDER, const wxString& name = wxPanelNameStr);
    void init();

    [[nodiscard]] wxTextCtrl* getSolutionTextControl() const;
    [[nodiscard]] wxStaticText *getNumberOfEdgesLabel() const;
    [[nodiscard]] wxStaticText *getNumberOfConceptsLabel() const;
    [[nodiscard]] wxStaticText *getNumberOfFacetsLabel() const;
    [[nodiscard]] wxStaticText *getNumberOfIndividualsLabel() const;
    [[nodiscard]] wxStaticText *getAddingLabel() const;
    [[nodiscard]] wxStaticText *getGroundingLabel() const;
    [[nodiscard]] wxStaticText *getExternalLabel() const;
    [[nodiscard]] wxStaticText *getSolvingLabel() const;
    [[nodiscard]] wxStaticText *getSumLabel() const;
    [[nodiscard]] wxStaticText *getQueryLabel() const;

    static void drawSolutionGraph(std::string path);

private:
    wxTextCtrl* solutionTextControl;
    // wxImagePanel* solutionGraph;
    wxBoxSizer* ontologyProgramTabSizer;
    wxStaticText* numberOfEdgesLabel;
    wxStaticText* numberOfConceptsLabel;
    wxStaticText* numberOfFacetsLabel;
    wxStaticText* numberOfIndividualsLabel;
    wxStaticText* addingLabel;
    wxStaticText* groundingLabel;
    wxStaticText* externalLabel;
    wxStaticText* solvingLabel;
    wxStaticText* sumLabel;
    wxStaticText* queryLabel;
};
