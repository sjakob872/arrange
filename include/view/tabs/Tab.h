#pragma once

#include <wx/panel.h>

class Tab : public wxPanel
{
public:
    explicit Tab(wxWindow* parent, wxWindowID winid = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
            long style = wxTAB_TRAVERSAL | wxNO_BORDER, const wxString& name = wxPanelNameStr);
    [[nodiscard]] bool isDirty() const;
    void setDirty(bool dirty);

private:
    bool dirty;
};
