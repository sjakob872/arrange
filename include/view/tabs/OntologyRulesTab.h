#pragma once

#include "view/tabs/Tab.h"

#include <wx/button.h>
#include <wx/textctrl.h>

class OntologyRulesTab : public Tab
{
public:
    explicit OntologyRulesTab(wxWindow* parent, wxWindowID winid = wxID_ANY, const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL | wxNO_BORDER, const wxString& name = wxPanelNameStr);

    void init();
    void enable();

    void OnSaveRulesButton(wxCommandEvent& event);

    [[nodiscard]] wxTextCtrl* getOntologyRulesTextControl() const;

private:
    wxTextCtrl* ontologyRulesTextControl;
    wxButton* saveRulesButton;
};