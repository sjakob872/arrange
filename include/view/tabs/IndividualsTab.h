#pragma once

#include "view/tabs/Tab.h"

#include <wx/button.h>
#include <wx/listctrl.h>

class IndividualsList;
class IArrangeController;
class IndividualsTab : public Tab
{
public:
    explicit IndividualsTab(wxWindow* parent, wxWindowID winid = wxID_ANY, const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL | wxNO_BORDER, const wxString& name = wxPanelNameStr);
    void init(IArrangeController* controller);
    void enable();

    void OnAddIndividualButton(wxCommandEvent& event);
    void OnEditIndividualButton(wxCommandEvent& event);
    void OnDeleteIndividualButton(wxCommandEvent& event);
    void OnSaveIndividualsButton(wxCommandEvent& event);

    [[nodiscard]] IndividualsList* getIndividualsListControl() const;

private:
    IArrangeController* arrangeController;

    IndividualsList* individualsListControl;
    wxButton* addIndividualButton;
    wxButton* editIndividualButton;
    wxButton* deleteIndividualButton;
    wxButton* saveIndividualsButton;
};
