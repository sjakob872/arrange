#pragma once

#include "model/interfaces/IOntologyController.h"
#include "view/interfaces/IArrangeController.h"

#include <iostream>
#include <string>

class MainFrame;
class OntologyManager;
class Individual;
class MainController : public IOntologyController, public IArrangeController
{
public:
    MainController();
    ~MainController();

    void createCN5Ontology(std::string& rootConcept) override;
    void createNewOntology() override;
    void loadOntology(std::string ontology) override;
    void ontologyCreated() override;

    void createIndividual(std::string individual, bool loaded) override;
    void createIndividual(std::string individualName, std::string baseConcept) override;
    void editIndividual(std::string individualOldName, std::string individualNewName, std::string baseConcept) override;
    void deleteIndividual(std::string individual) override;
    void individualCreated(Individual* individual, bool loaded, int individualsCount) override;
    void individualEdited(std::string individualOldName, Individual* individual) override;
    void individualDeleted(std::string individual, int individualsCount) override;

    void createEdge(std::string fromConcept, std::string relation, std::string toConcept, double weight) override;
    void editEdge(std::string oldStartConcept, std::string newStartConcept, std::string oldRelation, std::string newRelation,
            std::string oldEndConcept, std::string newEndConcept, double weight, long id) override;
    void deleteEdge(std::string startConcept, std::string relation, std::string endConcept, long id) override;
    void edgeCreated(std::string fromConcept, std::string relation, std::string toConcept, double weight, long id) override;
    void edgeEdited(std::string oldStartConcept, std::string oldRelation, std::string oldEndConcept, std::string newStartConcept,
            std::string newRelation, std::string newEndConcept, double weight, long id) override;
    void edgeDeleted(std::string startConcept, std::string relation, std::string endConcept, long id) override;

    void createFacets(std::vector<std::string> facets, bool translateSupplementary) override;
    void createFacet(std::string fromConcept, std::string relation, std::string toConcept, std::string typeString, std::string valueRange,
            int minCard, int maxCard, std::string individual, std::string value, long timeStep) override;
    void deleteFacet(std::string facet) override;
    void facetsCreated(std::vector<std::string> facets, bool dirty, int facetsCount) override;
    void facetDeleted(std::string facet, int facetsCount) override;

    void applySettings(
            double isAWeight, double formOfWeight, double synonymWeight, double hasPropertyWeight, double minRelatedness) override;

    void conceptCreated() override;

    std::vector<std::string> getAvailableConcepts() override;
    std::vector<std::string> getAvailableIndividuals() override;
    std::vector<std::string> getOntologyRelations() override;
    std::vector<std::string> getFacetRelations(bool skip) override;
    std::string getIndividualsASPProgram() override;
    std::pair<std::string,std::string> getFacetsASPProgram() override;
    std::string getOntologyProgram() override;
    std::vector<std::tuple<std::string, std::string, std::string, double, long>> getConnectedEdges(std::string conceptName) override;

    void setOntologyManager(OntologyManager* ontologyManager);
    void setMainFrame(MainFrame* mainFrame);

private:
    void generateConceptList();

    OntologyManager* ontologyManager;
    MainFrame* mainframe;
};
