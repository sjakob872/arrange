#pragma once

#include "view/interfaces/ISolverController.h"

#include <thread>
#include <vector>

class OntologyManager;
class MainFrame;
class SolveController : public ISolverController
{
public:
    SolveController();
    ~SolveController();

    void solve() override;
    std::vector<int> getStatistics() override;

    void setOntologyManager(OntologyManager* manager);
    void setMainFrame(MainFrame* mainframe);

private:
    OntologyManager* ontologyManager;
    MainFrame* mainFrame;
    std::thread* thread;
};
