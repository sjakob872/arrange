#pragma once

#define UNUSED_REL __attribute__((unused))

#define FACET_TYPES(TYPE)                                                                                                                  \
    TYPE(facetOf)                                                                                                                          \
    TYPE(typeOf)                                                                                                                           \
    TYPE(valueRangeOf)                                                                                                                     \
    TYPE(propertyViolation)                                                                                                                \
    TYPE(hasValue)                                                                                                                         \
    TYPE(subPropertyOf)                                                                                                                    \
    TYPE(domainOf)

// Enum
#define MAKE_ENUM(VAR) VAR,

enum FacetType
{
    FACET_TYPES(MAKE_ENUM)
};

// Strings
#define MAKE_STRINGS(VAR) #VAR,
UNUSED_REL static const char* facetTypes[] = {FACET_TYPES(MAKE_STRINGS)};
