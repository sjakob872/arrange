#pragma once

#include "model/Individual.h"

#include <condition_variable>
#include <map>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

namespace cnoe
{
class OntologyExtractor;
}

class Individual;
class Facet;
class FacetManager;
class IOntologyController;
class OntologyManager
{

public:
    OntologyManager();
    ~OntologyManager();

    void createCN5Ontology(std::string& rootConcept);
    void createNewOntology();
    void loadOntology(const std::string& ontology);

    void createIndividual(const std::string& individual, bool loaded);
    void createIndividual(const std::string& individualName, const std::string& individualConcept, bool loaded);
    void editIndividual(const std::string& individualOldName, const std::string& individualNewName, const std::string& baseConcept);
    void deleteIndividual(const std::string& individualName);

    void createEdge(const std::string& fromConcept, const std::string& relation, const std::string& toConcept, double weight);
    void editEdge(const std::string& oldStartConcept, const std::string& newStartConcept, const std::string& oldRelation,
            const std::string& newRelation, const std::string& oldEndConcept, const std::string& newEndConcept, double weight, long id);
    void deleteEdge(const std::string& fromConcept, const std::string& relation, const std::string& toConcept, long id);

    void createFacets(const std::vector<std::string>& facets, bool translateSupplementary);
    void createFacet(const std::string& fromConcept, const std::string& relation, const std::string& toConcept,
            const std::string& typeString, const std::string& valueRange, int minCard, int maxCard, const std::string& individual,
            const std::string& value, long timeStep);
    void deleteFacet(const std::string& facet);

    void setController(IOntologyController* controller);

    void conceptCreated();

    [[nodiscard]] cnoe::OntologyExtractor* getOntologyExtractor() const;
    [[nodiscard]] const std::map<std::string, Individual*>& getIndividuals() const;
    [[nodiscard]] std::vector<std::string> getFacets(bool translateSupplementary = true) const;
    [[nodiscard]] std::pair<std::string,std::string> getFacetsProgram() const;
    [[nodiscard]] std::vector<std::tuple<std::string, std::string, std::string, double, long>> getConnectedEdges(std::string conceptName);
    [[nodiscard]] std::vector<std::string> solve(std::string solutionPath);

private:
    void finished();
    [[nodiscard]] conceptnet::Concept* getConcept(const std::string& conceptName) const;

    std::map<std::string, Individual*> individuals;
    IOntologyController* controller;
    FacetManager* facetManager;
    cnoe::OntologyExtractor* ontologyExtractor;
    std::thread* extractionThread;
    std::thread* checkThread;
    std::mutex mtx;
    std::condition_variable cv;

    void cleanUp() const;
};
