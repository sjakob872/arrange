#pragma once

#include "model/FacetTypes.h"

#include <string>
#include <vector>

class Facet
{
public:
    friend class OntologyManager;
    friend class FacetManager;

    [[nodiscard]] const std::string& getTypeString() const;
    void setTypeString(const std::string& tString);
    [[nodiscard]] const std::string& getValueRange() const;
    void setValueRange(const std::string& range);
    [[nodiscard]] const std::string& getIndividualName() const;
    void setIndividualName(const std::string& individual);
    [[nodiscard]] const std::string& getValue() const;
    void setValue(const std::string& val);
    [[nodiscard]] const std::vector<std::string>& getSupplementaryRules() const;
    void setSupplementaryRule(const std::string& supplementaryRule);
    [[nodiscard]] int getMinCardinality() const;
    void setMinCardinality(int minCardinality);
    [[nodiscard]] int getMaxCardinality() const;
    void setMaxCardinality(int maxCardinality);
    [[nodiscard]] long getTimeStep() const;
    void setTimeStep(long timeStep);
    [[nodiscard]] std::string toString();

private:
    Facet(std::string firstConceptName, const std::string& type, std::string secondConceptName);
    static FacetType getFacetType(const std::string& facetType);
    static std::string trim(const std::string& str);
    std::string firstConceptName;
    std::string secondConceptName;
    std::string typeString;
    std::string valueRange;
    std::string individualName;
    std::string value;
    std::vector<std::string> supplementaryRules;
    FacetType type;
    int minCardinality;
    int maxCardinality;
    long timeStep;
};
