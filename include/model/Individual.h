#pragma once

#include <string>

namespace conceptnet
{
class Concept;
}

class Individual
{
public:
    friend class OntologyManager;
    [[nodiscard]] const std::string& getName() const;
    [[nodiscard]] const std::string& getBaseConceptName() const;
    [[nodiscard]] conceptnet::Concept* getBaseConcept() const;
    [[nodiscard]] const std::string& getBaseRelation() const;
    [[nodiscard]] int getWeight() const;

    bool operator<(Individual const& other) const { return name < other.getName(); }

private:
    Individual(std::string name, std::string baseConceptName, conceptnet::Concept* baseConcept, std::string baseRelation = "is",
            int weight = 1);

    std::string name;
    std::string baseConceptName;
    conceptnet::Concept* baseConcept;
    std::string baseRelation;
    int weight;
};
