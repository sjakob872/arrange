#pragma once

#include <vector>
#include <string>

class Individual;
class IOntologyController
{
public:
    virtual void ontologyCreated() = 0;

    virtual void individualCreated(Individual* individual, bool loaded, int individualsCount) = 0;
    virtual void individualEdited(std::string individualOldName, Individual* individual) = 0;
    virtual void individualDeleted(std::string individual, int individualsCount) = 0;

    virtual void edgeEdited(std::string oldStartConcept, std::string oldRelation, std::string oldEndConcept, std::string newStartConcept,
            std::string newRelation, std::string newEndConcept, double weight, long id) = 0;
    virtual void edgeDeleted(std::string startConcept, std::string relation, std::string endConcept, long id) = 0;
    virtual void edgeCreated(std::string fromConcept, std::string relation, std::string toConcept, double weight, long id) = 0;

    virtual void conceptCreated() = 0;

    virtual void facetsCreated(std::vector<std::string> facets, bool dirty, int facetsCount) = 0;
    virtual void facetDeleted(std::string facet, int facetsCount) = 0;
};
