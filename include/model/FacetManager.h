#pragma once

#include "model/FacetTypes.h"

#include <map>
#include <string>
#include <vector>

class Facet;
class OntologyManager;
class FacetManager
{
public:
    explicit FacetManager(OntologyManager* manager);
    ~FacetManager();
    void createFacet(const std::string& facet);
    void createFacet(const std::string& fromConcept, const std::string& relation, const std::string& toConcept,
            const std::string& typeString, const std::string& valueRange, int minCard, int maxCard, const std::string& individual,
            const std::string& value, long timeStep);
    void deleteFacet(const std::string& facet);

    [[nodiscard]] std::pair<std::string,std::string>translateFacets();
    [[nodiscard]] std::vector<std::string> getFacets(bool translateSupplementary = true);
    [[nodiscard]] int getNumberOfFacets();

    void cleanUp();

private:
    Facet* getOrCreateFacetOfFacet(const std::string& facet, const std::string& facetType);
    Facet* getOrCreateTypeOfFacet(const std::string& facet, const std::string& facetType);
    Facet* getOrCreateValueRangeOfFacet(const std::string& facet, const std::string& facetType);
    Facet* getOrCreateHasValueFacet(const std::string& facet, const std::string& facetType);
    Facet* getOrCreateSubPropertyOfFacet(const std::string& facet, const std::string& facetType);
    Facet* getOrCreateDomainOfFacet(const std::string& facet, const std::string& facetType);
    void attachSupplementaryRule(const std::string& facet);
    static void createSupplementaryRules(Facet* facet);

    OntologyManager* ontologyManager;
    std::map<FacetType, std::vector<Facet*>> facets;
};